package ua.khpi.oop.kostin14;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.stream.IntStream;

/**
 * Main task:
 * - Provide measurement of the time of parallel processing of container
 *   elements using previously developed methods.
 * - Add an artificial delay to the algorithms for each iteration of the
 *   element-by-element processing cycles so that the total processing time is
 *   a few seconds.
 * - Implement sequential processing of the container using the methods used for
 *   parallel processing and ensure the measurement of their operating time.
 * - Compare the time of parallel and sequential processing and draw conclusions
 *   about the effectiveness of parallelization:
 *   - summarize the results of time measurement;
 *   - calculate and demonstrate how many times parallel execution is faster
 *     than sequential execution.
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	private static final long SLEEP_TIME = 5;
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		// Array to process
		ArrayList<Route> routes = new ArrayList<>();
		fillRouteContainer(routes, 1000, 350);
		// Create tasks (Min/Max/Avg seats count)
		FutureTask<Integer> taskMinSeats = new FutureTask<>(new Callable<>() {
			@Override
			public Integer call() throws Exception {
				return getMinSeatsCount(routes, SLEEP_TIME);
			}
		});
		FutureTask<Integer> taskMaxSeats = new FutureTask<>(new Callable<>() {
			@Override
			public Integer call() throws Exception {
				return getMaxSeatsCount(routes, SLEEP_TIME);
			}
		});
		FutureTask<Double> taskAvgSeats = new FutureTask<>(new Callable<>() {
			@Override
			public Double call() throws Exception {
				return getAvgSeatsCount(routes, SLEEP_TIME);
			}
		});
		/// Create and run threads
		Thread threads[] = new Thread[3];
		threads[0] = new Thread(taskMinSeats);
		threads[1] = new Thread(taskMaxSeats);
		threads[2] = new Thread(taskAvgSeats);
		int answerThreadMin = -1;
		int answerThreadMax = -1;
		double answerThreadAvg = -1;
		System.out.println("Start parallel processing");
		long startTimeThreads = System.currentTimeMillis();
		IntStream.range(0, 3).forEach(i -> threads[i].start());
		// Wait for results/interruption
		try {
			answerThreadMin = taskMinSeats.get();
			answerThreadMax = taskMaxSeats.get();
			answerThreadAvg = taskAvgSeats.get();
		} catch (InterruptedException | ExecutionException e1) {
			e1.printStackTrace();
		}
		long elapsedTimeThreads = System.currentTimeMillis() - startTimeThreads;
		// Non-parallel test
		int answerMainMin = -1;
		int answerMainMax = -1;
		double answerMainAvg = -1;
		System.out.println("Start non-parallel processing");
		long startTimeMain = System.currentTimeMillis();
		try {
			answerMainMin = getMinSeatsCount(routes, SLEEP_TIME);
			answerMainMax = getMaxSeatsCount(routes, SLEEP_TIME);
			answerMainAvg = getAvgSeatsCount(routes, SLEEP_TIME);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long elapsedTimeMain = System.currentTimeMillis() - startTimeMain;
		// Results:
		System.out.printf("Parallel:\n-Time: %d\n-Results: %d, %d, %f\n",
				elapsedTimeThreads,
				answerThreadMin, answerThreadMax, answerThreadAvg);
		System.out.printf("Non-Parallel:\n-Time: %d\n-Results: %d, %d, %f\n",
				elapsedTimeMain,
				answerMainMin, answerMainMax, answerMainAvg);
	}
	
	/**
	 * Find minimum seats count
	 * @param routes Routes
	 * @param sleepTime Thread sleep time
	 * @return Minimum seats count
	 * @throws InterruptedException
	 */
	private static int getMinSeatsCount(List<Route> routes, long sleepTime)
			throws InterruptedException {
		int minCount = Integer.MAX_VALUE;
		for (Route route : routes) {
			Thread.sleep(sleepTime);
			if (route.getSeatsCount() < minCount) {
				minCount = route.getSeatsCount();
			}
		}
		return minCount;
	}
	
	/**
	 * Find maximum seats count
	 * @param routes Routes
	 * @param sleepTime Thread sleep time
	 * @return Maximum seats count
	 * @throws InterruptedException
	 */
	private static int getMaxSeatsCount(List<Route> routes, long sleepTime)
			throws InterruptedException {
		int maxCount = 0;
		for (Route route : routes) {
			Thread.sleep(sleepTime);
			if (route.getSeatsCount() > maxCount) {
				maxCount = route.getSeatsCount();
			}
		}
		return maxCount;
	}
	
	/**
	 * Find average seats count
	 * @param routes Routes
	 * @param sleepTime Thread sleep time
	 * @return Average seats count
	 * @throws InterruptedException
	 */
	private static double getAvgSeatsCount(List<Route> routes, long sleepTime)
			throws InterruptedException {
		double avg = 0;
		for (Route route : routes) {
			Thread.sleep(sleepTime);
			avg += route.getSeatsCount();
		}
		return avg / routes.size();
	}
	
	/**
	 * Fill list of routes with random data
	 * @param array Array of routes
	 * @param count Count of routes to add
	 * @param maxSeats Max seats count
	 */
	private static void fillRouteContainer(ArrayList<Route> array, int count,
			int maxSeats) {
		Objects.requireNonNull(array);
		Random rand = new Random();
		EnumSet<DayOfWeek> days = EnumSet.of(DayOfWeek.MONDAY);
		for (int i = 1; i <= count; ++i) {
			array.add(new Route(rand.nextInt(maxSeats), days, i));
		}
	}
}
