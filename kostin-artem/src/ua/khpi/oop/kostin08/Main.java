package ua.khpi.oop.kostin08;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Iterator;

/**
 * Main task:
 * - Provide the possibility of preserving and restoring the array of objects
 *   for solving the problem of laboratory work №7.
 * - The use of a standard serialization protocol is prohibited.
 * - Demonstrate the use of the Long Term Persistence model.
 * - Provide a dialogue with the user in the form of a simple text menu.
 * - When saving and restoring data, provide a dialog mode for selecting
 *   a directory with the display of content and the ability to move through
 *   subdirectories.
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/** Ticket office for demonstration */
	private static TicketOffice office = new TicketOffice();
	
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		/* Route to add */
		Route temp = new Route(250, EnumSet.of(DayOfWeek.MONDAY), 1);
		
		temp.addNode(new RouteNode("Kharkiv", new Time(),
				new Time(14, 30, 0), 250));
		temp.addNode(new RouteNode("Krakov", new Time(20, 14, 0),
				new Time(20, 45, 30), 120));
		temp.addNode(new RouteNode("Tarkov", new Time(8, 10, 45),
				new Time(), 0));
		office.addRoute(temp);
		
		temp = new Route(130, EnumSet.of(DayOfWeek.MONDAY,
				DayOfWeek.TUESTAY, DayOfWeek.WEDNESDAY,
				DayOfWeek.SATURDAY), 2);
		temp.addNode(new RouteNode("Kharkiv", new Time(),
				new Time(7, 30, 0), 130));
		temp.addNode(new RouteNode("Hogwarts", new Time(8, 30, 0),
				new Time(), 0));
		office.addRoute(temp);
		
		runDialogLoop();
	}
	
	/**
	 * Prints dialog options and handles the user's answer
	 */
	private static void runDialogLoop() {
		BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in));
		String line = null;
		
		while (true) {
			printDialogOpts();
			
			try {
				line = in.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line == null) {
				break;
			}
			
			switch (line) {
			case "1":
				System.out.println(office.toString());
				break;
			case "2":
			{
				File dir = chooseDir(".", in);
				if (dir == null) {
					System.out.println("Incorrect argument. Please try again");
					continue;
				}
				serialize(office, dir.getAbsolutePath());
				break;
			}
			case "3":
			{
				File dir = chooseDir(".", in);
				if (dir == null) {
					System.out.println("Incorrect argument. Please try again");
					continue;
				}
				Object obj = deserialize(dir.getAbsolutePath());
				if (obj == null) {
					System.out.
						println("Can't find a bean file in the current dir");
					continue;
				}
				office = (TicketOffice)obj;
				break;
			}
			default:
				System.out.println("Incorrect argument. Please try again");	
				break;
			}
		}
		
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Allows user to choose directory by moving from the start directory
	 * @param startDir Start directory
	 * @param in Input reader
	 * @return Chosen directory
	 */
	private static File chooseDir(String startDir, BufferedReader in) {
		if (startDir == null || in == null) {
			throw new NullPointerException();
		}
		
		File currFile = new File(startDir);
		if (!currFile.exists() || !currFile.isDirectory()) {
			return null;
		}
		currFile = new File(currFile.getAbsolutePath());
		
		while (true) {
			StringBuilder msg = new StringBuilder();
			msg.append("Current dir: ").append(currFile.getAbsolutePath());
			msg.append("\n1. Choose current directory\n")
				.append("2. Go back\n");
			
			ArrayList<File> filesList = new ArrayList<>(
					Arrays.asList(currFile.listFiles()));
			filesList.removeIf(file -> !file.isDirectory());

			Iterator<File> it = filesList.iterator();
			for (int i = 3; it.hasNext(); ++i) {
				msg.append(i).append(". ")
					.append(it.next().getName()).append('\n');
			}
			System.out.println(msg.toString());
			
			String line = null;
			try {
				line = in.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line == null) {
				return null;
			}
			
			switch (line) {
			case "1":
				return currFile;
			case "2":
				File parent = currFile.getParentFile();
				//System.out.println(currFile.get());
				if (parent == null || !parent.exists()) {
					System.out.println("There is no way back!");
					break;
				}
				currFile = parent;
				break;
			default:
				int fileNum = 0;
				try {
					fileNum = Integer.parseInt(line);					
				} catch (NumberFormatException e) {
					System.out.println("Incorrect number");
					continue;
				}
				fileNum -= 3; // 2 reserved + index starts with 0
				if (fileNum < 0 || fileNum >= filesList.size()) {
					System.out.println("Incorrect option");
					continue;
				}
				
				currFile = filesList.get(fileNum);
				continue;
			}
		}
	}
	
	/**
	 * Prints dialog options
	 */
	private static void printDialogOpts() {
		StringBuffer msg = new StringBuffer();
		msg.append("1. Print the list\n");
		msg.append("2. Serialize the list to a file\n");
		msg.append("3. Deserialize a list from a file\n");
		System.out.print(msg.toString());
	}
	
	/**
	 * Serializes object to the file "$(dir)/BeanObject.xml"
	 * @param obj Object to serialize
	 * @param dir Directory to place file into
	 */
	private static void serialize(Object obj, String dir) {
		if (obj == null) {
			return;
		}
		
		XMLEncoder encoder = null;
		try {
			encoder = new XMLEncoder(
			           new BufferedOutputStream(
			           new FileOutputStream(dir + "/BeanObject.xml")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		encoder.writeObject(obj);
		encoder.close(); 
	}
	
	/**
	 * Deserializes object from the file "$(dir)/BeanObject.xml"
	 * @param dir Directory with serialized file
	 * @return Deserialized object or null if it wasn't found
	 */
	private static Object deserialize(String dir) {
		XMLDecoder decoder = null;
		try {
			decoder = new XMLDecoder(
				    new BufferedInputStream(
				    new FileInputStream(dir + "/BeanObject.xml")));
		} catch (FileNotFoundException e) {
			return null;
		}

		Object object = decoder.readObject();
		decoder.close();
		return object;
	}
}
