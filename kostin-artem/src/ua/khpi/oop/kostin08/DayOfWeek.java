package ua.khpi.oop.kostin08;

import java.io.Serializable;

/**
 * Enum that represents days of week
 * @author Kostin A.S. CIT-120b
 */
public enum DayOfWeek implements Serializable {
	/** Sunday */
	SUNDAY("Воскресенье"),
	/** Monday */
	MONDAY("Понедельник"),
	/** Tuesday */
	TUESTAY("Вторник"),
	/** Wednesday */
	WEDNESDAY("Среда"),
	/** Thursday */
	THURSDAY("Четверг"),
	/** Friday */
	FRIDAY("Пятница"),
	/** Saturday */
	SATURDAY("Суббота");
	
	/** Name of the day of week */
	private String name;
	
	/**
	 * Constructor with parameters
	 * @param name Name of the day of week
	 */
	DayOfWeek(String name) {
		if (name == null) {
			throw new NullPointerException();
		}
		
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
