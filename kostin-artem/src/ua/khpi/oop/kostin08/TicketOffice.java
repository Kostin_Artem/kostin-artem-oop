package ua.khpi.oop.kostin08;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class that represents ticket office data
 * @author Kostin A.S. CIT-120b
 */
public final class TicketOffice implements Serializable {
	/** Serial ID for serialization */
	private static final long serialVersionUID = 1L;
	/** Array of available routes */
	private ArrayList<Route> routes;
	
	/** Constructor without parameters */
	public TicketOffice() {
		routes = new ArrayList<>();
	}
	
	/**
	 * Constructor with parameters
	 * @param initialCapacity Initial capacity of the array of routes
	 */
	public TicketOffice(int initialCapacity) {
		routes = new ArrayList<>(initialCapacity);
	}
	
	/**
	 * Add new route to the array
	 * @param route New route
	 */
	public void addRoute(Route route) {
		if (route == null) {
			throw new NullPointerException();
		}
		
		int index = routes.size();
		while (index > 0) {
			if (route.getRouteNumber()
				> routes.get(index - 1).getRouteNumber()) {
				break;
			}
			--index;
		}
		
		routes.add(index, route);
	}
	
	/**
	 * @return Copy of the routes array
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Route> getRoutes() {
		return (ArrayList<Route>) routes.clone();
	}
	
	/**
	 * Sets new array of routes
	 * @param routes New array
	 */
	@SuppressWarnings("unchecked")
	public void setRoutes(ArrayList<Route> routes) {
		if (routes == null) {
			throw new NullPointerException();
		}
		this.routes = (ArrayList<Route>) routes.clone();
	}
	
	@Override
	public String toString() {
		StringBuilder msg = new StringBuilder();
		msg.append("TicketOffice { routes {\n");
		for (Route route : routes) {
			msg.append(route.toString()).append('\n');
		}
		msg.append("} }");
		return msg.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
