package ua.khpi.oop.kostin08;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;

/**
 * Class that represents route data
 * @author Kostin A.S. CIT-120b
 */
public final class Route implements Serializable {
	/** Serial ID for serialization */
	private static final long serialVersionUID = 1L;
	
	/** Array of nodes of the route */
	private ArrayList<RouteNode> nodes;
	/** Total number of free seats */
	private int seatsCount;
	/** Days on which the route runs */
	private EnumSet<DayOfWeek> daysOfWeek;
	/** Number of the route */
	private int routeNumber;

	/** Constructor without parameters */
	public Route() {
		nodes = new ArrayList<>();
		daysOfWeek = EnumSet.noneOf(DayOfWeek.class);
	}
	
	/**
	 * Constructor with parameters
	 * @param seats Total number of free seats
	 * @param days Days on which the route runs
	 * @param routeNum Number of the route
	 */
	public Route(int seats, EnumSet<DayOfWeek> days, int routeNum) {
		nodes = new ArrayList<>();
		seatsCount = seats;
		daysOfWeek = days.clone(); // EnumSet is not final so is not immutable
		routeNumber = routeNum;
	}
	
	/**
	 * Copy constructor
	 * @param route Route for copying
	 */
	@SuppressWarnings("unchecked")
	public Route(Route route) {
		nodes = (ArrayList<RouteNode>) route.nodes.clone();
		seatsCount = route.seatsCount;
		daysOfWeek = EnumSet.copyOf(route.daysOfWeek);
		routeNumber = route.routeNumber;
	}
	
	/**
	 * Adds a RouteNode to the route
	 * @param node RouteNode to add
	 */
	public void addNode(RouteNode node) {
		if (node == null) {
			throw new NullPointerException();
		}
		
		nodes.add(node);
	}
	
	/**
	 * @return Route's nodes
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<RouteNode> getNodes() {
		return (ArrayList<RouteNode>) nodes.clone();
	}
	
	/**
	 * @return Seats count
	 */
	public int getSeatsCount() {
		return seatsCount;
	}
	
	/**
	 * @return Days on which the route runs
	 */
	public EnumSet<DayOfWeek> getDaysOfWeek() {
		return daysOfWeek.clone();
	}
	
	/**
	 * @return Number of the route
	 */
	public int getRouteNumber() {
		return routeNumber;
	}
	
	/**
	 * Sets new array of nodes
	 * @param nodes Array of nodes
	 */
	@SuppressWarnings("unchecked")
	public void setNodes(ArrayList<RouteNode> nodes) {
		if (nodes == null) {
			throw new NullPointerException();
		}
		this.nodes = (ArrayList<RouteNode>) nodes.clone();
	}
	
	/**
	 * Sets new seats count
	 * @param seatsCount Seats count
	 */
	public void setSeatsCount(int seatsCount) {
		this.seatsCount = seatsCount;
	}
	
	/**
	 * Sets days on which the route runs
	 * @param daysOfWeek Days on which the route runs
	 */
	public void setDaysOfWeek(EnumSet<DayOfWeek> daysOfWeek) {
		if (daysOfWeek == null) {
			throw new NullPointerException();
		}
		this.daysOfWeek = daysOfWeek.clone();
	}
	
	/**
	 * Sets new route number
	 * @param routeNumber
	 */
	public void setRouteNumber(int routeNumber) {
		this.routeNumber = routeNumber;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Route { ").append(nodes.toString()).append(", ")
		   .append(seatsCount).append(", ")
		   .append(Arrays.toString(daysOfWeek.toArray())).append(", ")
		   .append(routeNumber).append(" }");
		return str.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
