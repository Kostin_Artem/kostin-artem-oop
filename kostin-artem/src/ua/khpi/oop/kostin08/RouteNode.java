package ua.khpi.oop.kostin08;

import java.io.Serializable;

/**
 * Class that represents data of the route node
 * @author Kostin A.S. CIT-120b
 */
public final class RouteNode implements Serializable {
	/** Serial ID for serialization */
	private static final long serialVersionUID = 1L;
	
	/** Name of the station */
	private String stationName;
	/** Departure time */
	private Time departureTime;
	/** Arrival time */
	private Time arrivalTime;
	/** Free seats count on this node */
	private int freeSeatsCount;
	
	/** Constructor without parameters */
	public RouteNode() {
		stationName = new String();
		departureTime = new Time();
		arrivalTime = new Time();
	}
	
	/**
	 * Constructor with parameters
	 * @param name Name of the station
	 * @param dep Departure time
	 * @param arr Arrival time
	 * @param seats Free seats count on the route
	 */
	public RouteNode(String name, Time dep, Time arr, int seats) {
		if (name == null) {
			throw new NullPointerException();
		}
		if (seats < 0 || (dep == null && arr == null)) {
			throw new IllegalArgumentException();
		}
		
		stationName = name;
		departureTime = dep;
		arrivalTime = arr;
		freeSeatsCount = seats;
	}
	
	/**
	 * Copy constructor
	 * @param node RouteNode for copying
	 */
	public RouteNode(RouteNode node) {
		stationName = new String(node.stationName);
		departureTime = new Time(node.departureTime);
		arrivalTime = new Time(node.arrivalTime);
		freeSeatsCount = node.freeSeatsCount;
	}
	
	/**
	 * @return Station name
	 */
	public String getStationName() {
		return stationName;
	}
	
	/**
	 * @return Arrival time
	 */
	public Time getArrivalTime() {
		return new Time(arrivalTime);
	}
	
	/**
	 * @return Departure time
	 */
	public Time getDepartureTime() {
		return new Time(departureTime);
	}
	
	/**
	 * @return Free seats count
	 */
	public int getFreeSeatsCount() {
		return freeSeatsCount;
	}
	
	/**
	 * Sets new station name
	 * @param stationName Station name
	 */
	public void setStationName(String stationName) {
		this.stationName = new String(stationName);
	}
	
	/**
	 * Sets new arrival time
	 * @param arrivalTime Arrival time
	 */
	public void setArrivalTime(Time arrivalTime) {
		this.arrivalTime = new Time(arrivalTime);
	}
	
	/**
	 * Sets new departure time
	 * @param departureTime Departure time
	 */
	public void setDepartureTime(Time departureTime) {
		this.departureTime = new Time(departureTime);
	}
	
	/**
	 * Sets new value to the variable freeSeatsCount
	 * @param freeSeatsCount Free seats count
	 */
	public void setFreeSeatsCount(int freeSeatsCount) {
		this.freeSeatsCount = freeSeatsCount;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("RouteNode { \"").append(stationName).append("\", ")
		   .append(departureTime).append(", ").append(arrivalTime)
		   .append(", ").append(freeSeatsCount).append(" }");
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
