package ua.khpi.oop.kostin12;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serial;
import java.io.Serializable;
import java.util.Iterator;

/**
 * @author Kostin A.S. CS-920b
 * @param <T> Data type
 */
public final class MyContainer<T>
	implements Iterable<T>, Serializable,Externalizable {
	@Serial
	private static final long serialVersionUID = 7791970653314361946L;
	/** Size of the container */
	transient private int size;
	/** The first element of the array */
	transient private MyNode<T> head;
	/** The last element of the array */
	transient private MyNode<T> tail;
	
	/** Push element to the end of the array */
	public void pushBack(T element) {
		MyNode<T> node = new MyNode<T>(element, tail, null);
		++size;
		if (tail != null) {
			tail.setNext(node);
			tail = node;
			return;
		}
		
		head = tail = node;
	}
	
	/** Remove the last element of the array */
	public void removeBack() {
		if (tail == null) {
			return;
		}
		
		MyNode<T> beforeTail = tail.getPrev();
		if (beforeTail != null) {
			beforeTail.setNext(null);
			tail = beforeTail;
		} else {
			head = null;
			tail = null;
		}
		--size;
	}
	
	/**
	 * @param i Index of the element
	 * @return Element with index i
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public T get(int i) throws ArrayIndexOutOfBoundsException {
		return getNode(i).getElement();
	}
	
	/**
	 * @param i Index of the element
	 * @return Node with index i
	 * @throws ArrayIndexOutOfBoundsException
	 */
	private MyNode<T> getNode(int i) throws ArrayIndexOutOfBoundsException {
		if (i >= size || i < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}
		
		// Current element in the loop
		MyNode<T> curr = null;
		
		// Direction (forward)
		if (i <= size << 2) {
			curr = head;
			while (i-- > 0) {
				curr = curr.getNext();
			}
		// Direction (backward)
		} else {
			curr = tail;
			while (i++ != size) {
				curr = curr.getPrev();
			}
		}
		
		return curr;
	}
	
	/**
	 * Swaps elements with indices first and second
	 * Skips swapping if the indices are equal
	 * @param first Index of the first element
	 * @param second Index of the other element
	 */
	public void swap(int first, int second) {
		if (first == second) {
			return;
		}
		
		MyNode<T> f = getNode(first);
		MyNode<T> s = getNode(second);
		T temp = f.getElement();
		f.setElement(s.getElement());
		s.setElement(temp);
	}
	
	/**
	 * @return Elements count
	 */
	public int size() {
		return size;
	}
	
	/** Clear the array */
	public void clear() {
        // Clearing all of the links between nodes is unnecessary, but:
        // - Helps a generational GC if the discarded nodes inhabit
        //   more than one generation
        // - Is sure to free memory even if there is a reachable Iterator
		// (c) Josh Bloch
		for (MyNode<T> curr = head; curr != null; ) {
			MyNode<T> next = curr.getNext();
			curr.clear();
			curr = next;
		}
		
		head = tail = null;
		size = 0;
	}
	
	/** Transform container to an array */
	public Object[] toArray() {
		Object[] array = new Object[size];
		int i = 0;
		for (MyNode<T> curr = head; curr != null; curr = curr.getNext()) {
			array[i++] = curr.getElement();
		}
		return array;
	}
	
	@Override
	public String toString() {
		StringBuilder msg = new StringBuilder("MyContainer { ");
		for (MyNode<T> curr = head; curr != null;) {
			msg.append(curr.getElement().toString());
			curr = curr.getNext();
			if (curr != null) {
				msg.append(", ");
			}
		}
		return msg.append(" }").toString();
	}
	
	/** @return Return if container is empty */
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public MyIterator iterator() {
		return new MyIterator();
	}
	
	/** Deserialize object
	 * @param stream - Input stream 
	 */
	@SuppressWarnings("unchecked")
	@Serial
	private void readObject(ObjectInputStream stream)
			throws IOException, ClassNotFoundException {
		stream.defaultReadObject();
		
		int size = stream.readInt();
		for (int i = 0; i < size; ++i) {
			pushBack((T)stream.readObject());
		}
	}
	
	/** Serialize object
	 * @param stream - Output stream 
	 */
	@Serial
	private void writeObject(ObjectOutputStream stream)
			throws IOException {
		stream.defaultWriteObject();
		stream.writeInt(size);
		
		for (MyNode<T> curr = head; curr != null; curr = curr.getNext()) {
			stream.writeObject(curr.getElement());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void readExternal(ObjectInput in)
			throws IOException, ClassNotFoundException {
		int size = in.readInt();
		for (int i = 0; i < size; ++i) {
			pushBack((T)in.readObject());
		}
	}
	
	@Override
	public void writeExternal(ObjectOutput out)
			throws IOException {
		out.writeInt(size);
		for (MyNode<T> curr = head; curr != null; curr = curr.getNext()) {
			out.writeObject(curr.getElement());
		}
	}
	
	/** Iterator of my container */
	public final class MyIterator implements Iterator<T> {
		/** Next element */
		private MyNode<T> next;
		
		/** Default constructor */
		private MyIterator() {
			next = head;
		}
		
		/** @return If there is the next element */
		@Override
		public boolean hasNext() {
			return next != null;
		}
		
		/**
		 * @return The next object or null if it's absent
		 */
		@Override
		public T next() {
			if (next == null) {
				return null;
			}
			
			T element = next.getElement();
			next = next.getNext();
			return element;
		}
	}
}
