package ua.khpi.oop.kostin12;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.function.Predicate;

/** Filters routes going on Monday and Friday, in which Kharkiv
 * is a transit station
 * @author Kostin A.S. CS-920b
 */
public class KharkivTransitFilter implements Predicate<Route> {
	@Override
	public boolean test(Route obj) {
		if (!obj.getDaysOfWeek()
				.equals(EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.FRIDAY))) {
			return false;
		}
		
		ArrayList<RouteNode> nodes = obj.getNodes();
		if (nodes.size() <= 2) {
			// Not transit by a definition
			return false;
		}
		if (nodes.get(0).getStationName().equals("Kharkiv")) {
			return false;
		}
		if (nodes.get(nodes.size() - 1).getStationName().equals("Kharkiv")) {
			return false;
		}
		
		// Must have at least 1 Kharkiv station
		for (int i = 1; i < nodes.size() - 1; ++i) {
			if (nodes.get(i).getStationName().equals("Kharkiv")) {
				return true;
			}
		}
		
		return false;
	}
}
