package ua.khpi.oop.kostin12;

import java.io.Serializable;

/**
 * Class that represents time
 * @author Kostin A.S. CS-920b
 */
public final class Time implements Serializable {
	/** Serial ID for serialization */
	private static final long serialVersionUID = 1L;
	
	/** Hours [0; 23] */
	private int hours;
	/** Minutes [0; 59] */
	private int minutes;
	/** Seconds [0; 59] */
	private int seconds;
	
	/** Constructor without parameters */
	public Time() {
		this(0, 0, 0);
	}
	
	/**
	 * Constructor with parameters
	 * @param hours Hours [0; 23]
	 * @param minutes Minutes [0; 59]
	 * @param seconds Seconds [0; 59]
	 */
	public Time(int hours, int minutes, int seconds) {
		if (hours < 0 || hours > 23
			|| minutes < 0 || minutes > 59
			|| seconds < 0 || seconds > 59) {
			throw new IllegalArgumentException();
		}
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	
	/**
	 * Copy constructor
	 * @param time Time class for copying
	 */
	public Time(Time time) {
		hours = time.hours;
		minutes = time.minutes;
		seconds = time.seconds;
	}
	
	/**
	 * @return Hours
	 */
	public int getHours() {
		return hours;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(hours).append(':').append(minutes)
		   .append(':').append(seconds);
		return str.toString();
	}
}
