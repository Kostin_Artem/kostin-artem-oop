package ua.khpi.oop.kostin12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main task:
 * - Using programs for solving previous problems, demonstrate the effective
 *   (optimal) use of regular expressions in solving an applied problem.
 * - Anticipate the possibility of a slight change in search conditions.
 * - Demonstrate developed functionality in dialog and automatic modes.
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	/** Path to the data file */
	private static final String S_FILE = "kostin12auto.dat";
	
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		boolean auto = false;
		if (args.length == 1 && args[0].equals("-auto")) {
			auto = true;
		} else if (args.length > 1){
			System.out.println("Usage: java <program> [-auto]");
			return;
		}
		
		if (!auto) {
			demoSolutionAuto(new KharkivTransitFilter());
		} else {
			demoSolution(new KharkivTransitFilter());
		}
	}
	
	/**
	 * Demonstrates solution in a dialog mode
	 */
	private static void demoSolution(Predicate<Route> routePredicate) {
		System.out.println("Main.demoSolution()");
		MyContainer<Route> cont = new MyContainer<>();
		
		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in))) {
			System.out.println("Print routes to test:");
			
			loop0:
			while (true) {
				String line = in.readLine();
				if (line == null) {
					break;
				}
				
				Route route = null;
				try {
					route = parseRoute(line);
				} catch (IllegalArgumentException e) {
					continue;
				}
				while (true) {
					line = in.readLine();
					if (line == null || line.isBlank()) {
						break;
					}
					
					try {
						route.addNode(parseRouteNode(line));
					} catch (IllegalArgumentException e) {
						// Incorrect node or time.
						continue loop0;
					}
				}
				if (routePredicate.test(route)) {
					cont.pushBack(route);
				}
			}
			
			System.out.println("Container data:");
			cont.forEach((route) -> {System.out.println(route);});
			
			System.out.println("Exiting...");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Parses Route from line using RegEx
	 * @param line Line to parse data from
	 * @return Route with data from the line
	 * @throws IllegalArgumentException if data is incorrect
	 */
	public static Route parseRoute(String line)
			throws IllegalArgumentException {
		// Route base data pattern
		Pattern routePattern = Pattern.compile("^(\\d+),\\s(\\d+)(?:,\\s"
				+ "(?:(Sunday)|(Monday)|(Tuesday)|(Wednesday)|(Thursday)|"
				+ "(Friday)|(Saturday)))+\\s?$");
		// Initial values of route (May be edited)
		int number = 0;
		int seats = 0;
		
		/* Check for pattern */
		Matcher mch = routePattern.matcher(line);
		if (!mch.matches()) {
			throw new IllegalArgumentException("Wrong route format!");
		}
		
		// Try to parse numbers
		try {
			number = Integer.parseInt(mch.group(1));
			seats = Integer.parseInt(mch.group(2));		
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Wrong route format!");
		}
		// Parse days
		ArrayList<DayOfWeek> toInclude = new ArrayList<>(7);
		for (int i = 3; i < 10; ++i) {
			if (mch.group(i) != null) {
				toInclude.add(DayOfWeek.values()[i-3]);
			}
		}
		
		// Create route
		return new Route(seats, EnumSet.copyOf(toInclude), number);
	}
	
	/**
	 * Parses RouteNode from line using RegEx
	 * @param line Line to parse data from
	 * @return RouteNode with data from the line
	 * @throws IllegalArgumentException if data is incorrect
	 */
	public static RouteNode parseRouteNode(String line)
			throws IllegalArgumentException {
		// Route base data pattern. Only for hours between [18; 23]
		Pattern nodePattern = Pattern.compile("^([A-Z][a-z]*),\\s"
				+ "(?:(?:(1[89]|2[0-3]):)([0-5]\\d):)([0-5]\\d)"
				+ ",\\s(?:(?:(1[89]|2[0-3]):)([0-5]\\d):)([0-5]\\d)"
				+ ",\\s(\\d+)\\s?$");
		
		/* Check for pattern */
		Matcher mch = nodePattern.matcher(line);
		if (!mch.matches()) {
			throw new IllegalArgumentException("Wrong route node format!");
		}
		
		// Parse time
		String stationName = mch.group(1);
		Time depTime = new Time(
				Integer.parseInt(mch.group(2)),
				Integer.parseInt(mch.group(3)),
				Integer.parseInt(mch.group(4)));
		Time arrTime = new Time(
				Integer.parseInt(mch.group(5)),
				Integer.parseInt(mch.group(6)),
				Integer.parseInt(mch.group(7)));
		int stationSeats = 0;
		try {
			stationSeats = Integer.parseInt(mch.group(8));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Wrong route node format!");
		}
		
		// Create and add a new node
		return new RouteNode(stationName, depTime, arrTime, stationSeats);
	}
	
	/**
	 * Demonstrates solution in an automatic mode
	 */
	private static void demoSolutionAuto(Predicate<Route> routePredicate) {
		System.out.println("Main.demoSolutionAuto()");
		MyContainer<Route> cont = new MyContainer<>();
		
		System.out.println("Reading data from file " + S_FILE);
		try (BufferedReader in = new BufferedReader(
				new FileReader(new File(S_FILE)))) {
			loop0:
			while (true) {
				String line = in.readLine();
				if (line == null) {
					break;
				}
				
				Route route = null;
				try {
					route = parseRoute(line);
				} catch (IllegalArgumentException e) {
					continue;
				}
				
				while (true) {
					line = in.readLine();
					if (line == null || line.isBlank()) {
						break;
					}
					
					try {
						route.addNode(parseRouteNode(line));						
					} catch (IllegalArgumentException e) {
						// Incorrect node or time.
						continue loop0;
					}
				}
				if (routePredicate.test(route)) {
					cont.pushBack(route);					
				}
			}
		} catch (IOException | IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		System.out.println("Container data:");
		cont.forEach((route) -> {System.out.println(route);});
		
		System.out.println("Exiting...");
	}
}
