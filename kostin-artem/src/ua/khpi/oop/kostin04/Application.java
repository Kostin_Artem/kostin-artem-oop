package ua.khpi.oop.kostin04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

import ua.khpi.oop.kostin03.IoHelper;
import ua.khpi.oop.kostin03.Pair;

/**
 * Class with the main dialog loop
 * @author Kostin A.S. CIT-120b
 */
public class Application {
	/** Is debug mode enabled */
	private boolean isDebug = false;
	
	/**
	 * Runs main dialog loop
	 */
	public void runLoop() {
		/* Reader to read the data */
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));
		/* Map with entered words */
		HashMap<String, Integer> wordsCount = new HashMap<>();
		/* A result of input (Numbers of the dialog menu) */
		String readResult = null;
		/* A result of input (Text) */
		String text = null;
		
		while (true) {
			printMenu();
			
			try {
				readResult = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (readResult == null) {
				System.out.println("The end of stream was encountered.");
				System.out.println("Exiting...");
				return;
			}
			
			switch (readResult) {
			case "1":
				text = handleEnter(reader);
				break;
			case "2":
				handlePrintData(text);
				break;
			case "3":
				handleProcessData(text, wordsCount);
				break;
			case "4":
				IoHelper.printAsTable(
						new Pair<String, String>("Word name", "Count"),
						wordsCount,
						System.out);
				break;
			case "5":
				System.out.println("Exiting...");
				return;
			default:
				System.out.println("Incorrect argument. Please try again");
				continue;
			}
		}
	}
	
	/**
	 * Parses command line args and sets the state of the application
	 * @param args Command line arguments
	 * @return Status of parsing
	 */
	public boolean parseArgs(String[] args) {
		/* Should print help message after parsing */
		boolean printHelp = false;
		for (int i = 0; i < args.length; ++i) {
			if (args[i].equals("-d") || args[i].equals("-debug")) {
				isDebug = true;
			} else if (args[i].equals("-h") || args[i].equals("-help")) {
				printHelp = true;
			} else {
				System.out.print("Incorrect argument: ");
				System.out.println(args[i]);
				printUsage();
				return false;
			}
		}
		if (printHelp) {
			printHelp();
		}
		return true;
	}
	
	/**
	 * Handles "Input data" option from the dialog menu
	 * @param reader Reader to input data
	 * @return Entered text
	 */
	private String handleEnter(BufferedReader reader) {
		/* Variable for text that will be read */
		String text = null;
		try {
			text = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (isDebug) {
			System.out.print("[DEBUG]: You have entered: ");
			if (text == null) {
				System.out.println("End of stream");
			} else {
				System.out.println(text);
			}
		}
		return text;
	}
	
	/**
	 * Handles "Print data" option from the dialog menu
	 * @param text Text to be printed
	 */
	private void handlePrintData(String text) {
		if (text == null) {
			System.out.println("You have entered nothing.");
		} else {
			StringBuilder msg = new StringBuilder();
			msg.append("You have entered: \"");
			msg.append(text);
			msg.append("\"\n");
			
			System.out.print(msg.toString());
		}
	}
	
	/**
	 * Parses text to a map of words
	 * @param text Text that will be parsed
	 * @param map Map to store the words from text
	 */
	private void handleProcessData(String text, HashMap<String, Integer> map) {
		if (text == null) {
			System.out.println("You have entered nothing.");
			return;
		}
		
		if (isDebug) {
			System.out.println("[DEBUG]: Handling the data");
		}
		
		{
			/* Words parser */
			StringTokenizer tokenizer = new StringTokenizer(text);
			map.clear();
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				map.put(token, map.getOrDefault(token, 0) + 1);
			}
		}
	}
	
	/**
	 * Prints a dialog menu
	 */
	private void printMenu() {
		StringBuilder msg = new StringBuilder();
		msg.append("1) Enter data\n");
		msg.append("2) Print data\n");
		msg.append("3) Process data\n");
		msg.append("4) Print the result\n");
		msg.append("5) Exit\n");
		
		System.out.print(msg.toString());
	}
	
	/**
	 * Prints a help message
	 */
	private void printHelp() {
		StringBuilder msg = new StringBuilder();
		msg.append("Author: Kostin A.S. CIT-120b\n");
		msg.append("Purpose: Demonstration of laboratory work #4\n");
		System.out.print(msg.toString());
		printUsage();
		msg.setLength(0);
		msg.append("Menu items:\n");
		msg.append(" -Enter data - Data entry for processing ");
		msg.append("(Only input, not processing)\n");
		msg.append(" -Print data - Prints the entered data\n");
		msg.append(" -Process data - Process the entered data ");
		msg.append("(Determine the number of specific words)\n");
		msg.append(" -Print the result - Print the result of processing ");
		msg.append("(If the data was previously entered and processed)\n");
		msg.append(" -Exit - Exit the program\n\n");
		System.out.print(msg.toString());
	}
	
	/**
	 * Prints usage of the program
	 */
	private void printUsage() {
		StringBuilder msg = new StringBuilder();
		msg.append("Usage: java -jar <program> [-d | -debug] [-h | -help]\n");
		msg.append("Arguments:\n");
		msg.append(" -debug - Display debug information\n");
		msg.append(" -d - The same as -debug\n");
		msg.append(" -help - Show information about the author ");
		msg.append("and description of menu items\n");
		msg.append(" -h - The same as -help\n");
		
		System.out.print(msg.toString());
	}
}
