package ua.khpi.oop.kostin04;

/**
 * Main task:
 * - Create dialog menu with next options:
 *  - Input data
 *  - Print the entered data
 *  - Handle data
 *  - Print the result
 *  - Exit etc..
 * - Handle next command line arguments:
 *  - -help - Print a help message
 *  - -debug - The program will print debug messages during processing
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		/* Class with the main dialog loop */
		Application app = new Application();
		if (!app.parseArgs(args)) {
			return;
		}
		app.runLoop();
	}
}
