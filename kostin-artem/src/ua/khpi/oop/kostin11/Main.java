package ua.khpi.oop.kostin11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main task:
 * - Demonstrate the effective use of regular expressions to verify the
 *   correctness of the data entered before writing to domain objects
 *   in accordance with the purpose of each field
 *   to fill the developed container:
 *   - When reading data from a text file in automatic mode.
 *   - When the user enters data in dialog mode.
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	/** Path to the data file */
	private static final String S_FILE = "kostin11auto.dat";
	
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		boolean auto = false;
		if (args.length == 1 && args[0].equals("-auto")) {
			auto = true;
		} else if (args.length > 1){
			System.out.println("Usage: java <program> [-auto]");
			return;
		}
		
		if (auto) {
			demoSolutionAuto();
		} else {
			demoSolution();
		}
	}
	
	/**
	 * Demonstrates solution in a dialog mode
	 */
	private static void demoSolution() {
		System.out.println("Main.demoSolution()");
		MyContainer<Route> cont = new MyContainer<>();
		
		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in))) {
			// Main dialog loop
			loop0:
			while (true) {
				printDlg0Options();
				String line = in.readLine();
				if (line == null) {
					System.out.println("End of stream. Exiting...");
					break;
				}
				
				switch (line) {
				// Print
				case "1": {
					cont.forEach((route) -> {System.out.println(route);});
					break;
				}
				// Add route
				case "2": {
					if (handleAddRoute(cont, in)) {
						break loop0;
					}
					break;
				}
				// Exit
				case "3": {
					System.out.println("Exiting...");
					break loop0;
				}
				default:
					System.out.println("Illegal argument.");
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Handles route creation
	 * @param cont Container to add route into
	 * @param in User input stream
	 * @return true if end of stream encountered
	 * @throws IOException
	 */
	private static boolean handleAddRoute(MyContainer<Route> cont,
			BufferedReader in) throws IOException {
		// Initial values of route (May be edited)
		// Route to create
		Route route = null;
		// Input line
		String line = null;
		
		// Ask to enter route base values
		while (true) {
			System.out.println("Enter the base route values "
					+ "(<number>, <seats count>, <day of weak>, ...):");
			line = in.readLine();
			if (line == null) {
				System.out.println("End of stream. Exiting...");
				return true;
			}
			
			// Try to parse route
			try {				
				route = parseRoute(line);
			} catch (IllegalArgumentException e) {
				System.out.println("Wrong input. Try again!");
				continue;
			}
			break;
		}
		
		// Route editing loop
		loop1:
		while (true) {
			printDlg1Options();
			line = in.readLine();
			if (line == null) {
				System.out.println("End of stream. Exiting...");
				return true;
			}
			
			switch (line) {
			// Edit number
			case "1": {
				System.out.println(route.toString());
				break;
			}
			// Edit seats
			case "2": {
				System.out.println("Enter the base station values "
						+ "(<Name>, <Dep. time>(11:11:11), "
						+ "<Arr. time>(11:11:11), <Seats count>):");
				line = in.readLine();
				if (line == null) {
					System.out.println("End of stream. Exiting...");
					return true;
				}
				
				try {
					route.addNode(parseRouteNode(line));
				} catch (IllegalArgumentException e) {
					System.out.println("Wrong route node format!");
				}
				break;
			}
			// Add to the container
			case "3": {
				cont.pushBack(route);
				System.out.println("New route added.");
				break loop1;
			}
			default:
				System.out.println("Illegal argument.");
				break;
			}
		}
		
		return false;
	}
	
	/**
	 * Parses Route from line using RegEx
	 * @param line Line to parse data from
	 * @return Route with data from the line
	 * @throws IllegalArgumentException if data is incorrect
	 */
	public static Route parseRoute(String line)
			throws IllegalArgumentException {
		// Route base data pattern
		Pattern routePattern = Pattern.compile("^(\\d+),\\s(\\d+)(?:,\\s"
				+ "(?:(Sunday)|(Monday)|(Tuesday)|(Wednesday)|(Thursday)|"
				+ "(Friday)|(Saturday)))+\\s?$");
		// Initial values of route (May be edited)
		int number = 0;
		int seats = 0;
		
		/* Check for pattern */
		Matcher mch = routePattern.matcher(line);
		if (!mch.matches()) {
			throw new IllegalArgumentException("Wrong route format!");
		}
		
		// Try to parse numbers
		try {
			number = Integer.parseInt(mch.group(1));
			seats = Integer.parseInt(mch.group(2));		
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Wrong route format!");
		}
		// Parse days
		ArrayList<DayOfWeek> toInclude = new ArrayList<>(7);
		for (int i = 3; i < 10; ++i) {
			if (mch.group(i) != null) {
				toInclude.add(DayOfWeek.values()[i-3]);
			}
		}
		
		// Create route
		return new Route(seats, EnumSet.copyOf(toInclude), number);
	}
	
	/**
	 * Parses RouteNode from line using RegEx
	 * @param line Line to parse data from
	 * @return RouteNode with data from the line
	 * @throws IllegalArgumentException if data is incorrect
	 */
	public static RouteNode parseRouteNode(String line)
			throws IllegalArgumentException {
		// Route base data pattern
		Pattern nodePattern = Pattern.compile("^([A-Z][a-z]*),\\s"
				+ "(?:(?:([01]?\\d|2[0-3]):)?([0-5]?\\d):)?([0-5]?\\d)"
				+ ",\\s(?:(?:([01]?\\d|2[0-3]):)?([0-5]?\\d):)?([0-5]?"
				+ "\\d),\\s(\\d+)\\s?$");
		
		/* Check for pattern */
		Matcher mch = nodePattern.matcher(line);
		if (!mch.matches()) {
			System.out.println("Wrong input!");
			throw new IllegalArgumentException("Wrong route node format!");
		}
		
		// Parse time
		String stationName = mch.group(1);
		Time depTime = new Time(
				Integer.parseInt(mch.group(2)),
				Integer.parseInt(mch.group(3)),
				Integer.parseInt(mch.group(4)));
		Time arrTime = new Time(
				Integer.parseInt(mch.group(5)),
				Integer.parseInt(mch.group(6)),
				Integer.parseInt(mch.group(7)));
		int stationSeats = 0;
		try {
			stationSeats = Integer.parseInt(mch.group(8));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Wrong route node format!");
		}
		
		// Create and add a new node
		return new RouteNode(stationName, depTime, arrTime, stationSeats);
	}
	
	/**
	 * Prints options of the main dialog
	 */
	private static void printDlg0Options() {
		StringBuilder msg = new StringBuilder();
		msg.append("1) Print the container\n");
		msg.append("2) Add new route to the container\n");
		msg.append("3) Exit\n");
		System.out.print(msg.toString());
	}
	
	/**
	 * Prints options of the edit dialog
	 */
	private static void printDlg1Options() {
		StringBuilder msg = new StringBuilder();
		msg.append("1) Print the route\n");
		msg.append("2) Add new route node\n");
		msg.append("3) Apply and add to the container\n");
		System.out.print(msg.toString());
	}
	
	/**
	 * Demonstrates solution in an automatic mode
	 */
	private static void demoSolutionAuto() {
		System.out.println("Main.demoSolutionAuto()");
		MyContainer<Route> cont = new MyContainer<>();
		
		System.out.println("Reading data from file " + S_FILE);
		try (BufferedReader in = new BufferedReader(
				new FileReader(new File(S_FILE)))) {
			while (true) {
				String line = in.readLine();
				if (line == null || line.isBlank()) {
					break;
				}
				
				Route route = parseRoute(line);
				while (true) {
					line = in.readLine();
					if (line == null || line.isBlank()) {
						break;
					}
					
					route.addNode(parseRouteNode(line));
				}
				cont.pushBack(route);
			}
		} catch (IOException | IllegalArgumentException e) {
			e.printStackTrace();
		}
		
		System.out.println("Container data:");
		cont.forEach((route) -> {System.out.println(route);});
		
		System.out.println("Exiting...");
	}
}
