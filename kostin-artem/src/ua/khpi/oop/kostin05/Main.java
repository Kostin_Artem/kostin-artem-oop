package ua.khpi.oop.kostin05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Main task:
 * - Create dialog menu with next options:
 *  - Input data
 *  - Print the entered data
 *  - Handle data
 *  - Print the result
 *  - Exit etc..
 * - Handle next command line arguments:
 *  - -help - Print a help message
 *  - -debug - The program will print debug messages during processing
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		/* Container with the text lines */
		MyContainer lines = new MyContainer();
		/* Map with count of words in the text */
		HashMap<String, Integer> wordsCount = new HashMap<>();
		/* Input reader */
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));
		
		/* Parse lines while not end of stream */
		while (true) {
			String line = null;
			try {
				line = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line == null) {
				if (lines.size() == 0) {
					return;
				}
				break;
			}
			lines.add(line);
		}
		
		System.out.println(lines.toString());
		
		/* Parse words */
		for (String line : lines) {
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				wordsCount.put(token, wordsCount.getOrDefault(token, 0) + 1);
			}
		}
		
		IoHelper.printAsTable(
				new Pair<String, String>("Word name", "Count"),
				wordsCount,
				System.out);
	}
}
