package ua.khpi.oop.kostin15.dialog;

import java.util.ArrayList;

/**
 * Dialog option handler
 * @author Kostin A.S. CS-920b
 */
@FunctionalInterface
public interface DialogOption {
	/**
	 * Handle dialog option if it was selected
	 * @param params Stack with parameters
	 * @throws Exception
	 */
	public void handleOption(ArrayList<Object> params) throws Exception;
}
