package ua.khpi.oop.kostin15.dialog;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Objects;

import ua.khpi.oop.kostin15.Pair;

/**
 * Standard dialog menu handler
 * @author Kostin A.S. CS-920b
 */
public class DialogHandler implements DialogOption {
	/** Options to show and handle */
	private ArrayList<Pair<String, DialogOption>> options;
	
	public DialogHandler() {
		options = new ArrayList<>();
	}

	@Override
	public void handleOption(ArrayList<Object> params) throws Exception {
		/* Retrieve params from the stack */
		BufferedReader in = (BufferedReader)params.get(0);
		while (true) {
			printOptions();
			/* Retrieve the option number from user */
			String input = in.readLine();
			if (input == null) {
				return;
			}
			int number = 0;
			try {
				number = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Incorrect number format. Try again");
				continue;
			}
			if (number < 1 || number > options.size() + 1) {
				System.out.println("Incorrect option number. Try again");
				continue;
			} else if (number == options.size() + 1) {
				/* Execute OnExit handler */
				onExit();
				return;
			}
			/* Execute the handler of the corresponding option */
			options.get(number - 1).getSecond().handleOption(params);
		}
	}
	
	/**
	 * Exit handler. Executed when user selected the exit option
	 */
	protected void onExit() {}
	
	/**
	 * Register an option
	 * @param name Name of the option to be displayed in the menu
	 * @param option Option handler
	 */
	public void addOption(String name, DialogOption option) {
		Objects.requireNonNull(name);
		Objects.requireNonNull(option);
		options.add(new Pair<>(name, option));
	}
	
	/**
	 * Print option names, their numbers, and the exit option
	 */
	private void printOptions() {
		StringBuffer buf = new StringBuffer();
		buf.append("Options:\n");
		int i = 0;
		for (; i < options.size(); ++i) {
			buf.append(i + 1).append(") ")
				.append(options.get(i).getFirst())
				.append('\n');
		}
		buf.append(i + 1).append(") ")
			.append("Exit\n");
		System.out.print(buf.toString());
	}
}
