package ua.khpi.oop.kostin15;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class that represents data of the route node
 * @author Kostin A.S. CS-920b
 */
public final class RouteNode implements Serializable {
	private static final long serialVersionUID = -1758179291238541572L;
	/** Name of the station */
	private String stationName;
	/** Departure time */
	private Time departureTime;
	/** Arrival time */
	private Time arrivalTime;
	/** Free seats count on this node */
	private int freeSeatsCount;
	
	/** Constructor without parameters */
	public RouteNode() {
		stationName = new String();
		departureTime = new Time();
		arrivalTime = new Time();
	}
	
	/**
	 * Constructor with parameters
	 * @param name Name of the station
	 * @param dep Departure time
	 * @param arr Arrival time
	 * @param seats Free seats count on the route
	 */
	public RouteNode(String name, Time dep, Time arr, int seats) {
		Objects.requireNonNull(name);
		if (seats < 0 || (dep == null && arr == null)) {
			throw new IllegalArgumentException();
		}
		
		stationName = name;
		departureTime = dep;
		arrivalTime = arr;
		freeSeatsCount = seats;
	}
	
	/**
	 * Copy constructor
	 * @param node RouteNode for copying
	 */
	public RouteNode(RouteNode node) {
		stationName = new String(node.stationName);
		departureTime = new Time(node.departureTime);
		arrivalTime = new Time(node.arrivalTime);
		freeSeatsCount = node.freeSeatsCount;
	}
	
	/**
	 * @return Name of the station
	 */
	public String getStationName() {
		return stationName;
	}
	
	/**
	 * @return Arrival time
	 */
	public Time getArrivalTime() {
		return arrivalTime;
	}
	
	/**
	 * @return Departure time
	 */
	public Time getDepartureTime() {
		return departureTime;
	}
	
	/**
	 * @return Free seats count
	 */
	public int getFreeSeatsCount() {
		return freeSeatsCount;
	}
	
	/**
	 * Sets new station name
	 * @param stationName Station name
	 */
	public void setStationName(String stationName) {
		Objects.requireNonNull(stationName);
		this.stationName = new String(stationName);
	}
	
	/**
	 * Sets new arrival time
	 * @param arrivalTime Arrival time
	 */
	public void setArrivalTime(Time arrivalTime) {
		Objects.requireNonNull(arrivalTime);
		this.arrivalTime = new Time(arrivalTime);
	}
	
	/**
	 * Sets new departure time
	 * @param departureTime Departure time
	 */
	public void setDepartureTime(Time departureTime) {
		Objects.requireNonNull(departureTime);
		this.departureTime = new Time(departureTime);
	}
	
	/**
	 * Sets new value to the variable freeSeatsCount
	 * @param freeSeatsCount Free seats count
	 */
	public void setFreeSeatsCount(int freeSeatsCount) {
		if (freeSeatsCount < 0) {
			throw new IllegalArgumentException();
		}
		this.freeSeatsCount = freeSeatsCount;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("RouteNode { \"").append(stationName).append("\", ")
		   .append(departureTime).append(", ").append(arrivalTime)
		   .append(", ").append(freeSeatsCount).append(" }");
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RouteNode) {
			RouteNode node = ((RouteNode) obj);
			return node.stationName.equals(stationName)
					&& node.departureTime.equals(departureTime)
					&& node.arrivalTime.equals(arrivalTime)
					&& node.freeSeatsCount == freeSeatsCount;
		}
		return false;
	}
}
