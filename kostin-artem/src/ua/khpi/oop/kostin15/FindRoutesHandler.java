package ua.khpi.oop.kostin15;

/**
 * Dialog-mode handler for the "Find routes by days of week" option
 * @author Kostin A.S. CS-920b
 */
public class FindRoutesHandler extends DaysOfWeekHandler {
	/**
	 * Constructor without parameters
	 */
	public FindRoutesHandler() {
		selectedDays = 0;
		addOption("Find all", params -> {
			/* Retrieve params from the stack */
			TicketOffice office = (TicketOffice)params.get(1);
			/* Find routes */
			System.out.println(office.findRoutesByDays(selectedToEnum()));
		});
	}
	
	@Override
	protected void onExit() {
		selectedDays = 0;
	}
}
