package ua.khpi.oop.kostin15;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Objects;

/**
 * Class that represents ticket office data
 * @author Kostin A.S. CS-920b
 */
public final class TicketOffice implements Serializable {
	private static final long serialVersionUID = 5116483942524266722L;
	/** Array of available routes */
	private ArrayList<Route> routes;
	
	/** Constructor without parameters */
	public TicketOffice() {
		routes = new ArrayList<>();
	}
	
	/**
	 * @return Array of available routes
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Route> getRoutes() {
		return (ArrayList<Route>)routes.clone();
	}
	
	/**
	 * Set array of available routes
	 * @param routes Array of available routes
	 */
	@SuppressWarnings("unchecked")
	public void setRoutes(ArrayList<Route> routes) {
		Objects.requireNonNull(routes);
		this.routes = (ArrayList<Route>)routes.clone();
	}
	
	/**
	 * Add new route to the array
	 * @param route New route
	 */
	public void addRoute(Route route) {
		Objects.requireNonNull(route);
		routes.add(route);
	}
	
	/**
	 * Remove route from the list
	 * @param id Route's id index the list
	 */
	public void removeRoute(int index) {
		routes.remove(index);
	}
	
	/**
	 * Find all routes that are available on the specified days of the week
	 * @param daysOfWeek Days of the week
	 * @return All routes that are available
	 */
	public ArrayList<Route> findRoutesByDays(EnumSet<DayOfWeek> daysOfWeek) {
		ArrayList<Route> result = new ArrayList<>();
		routes.forEach(route -> {
			for (DayOfWeek day : daysOfWeek) {
				if (!route.getDaysOfWeek().contains(day)) {
					/* Continue */
					return;
				}
			}
			result.add(route);
		});
		return result;
	}
	
	/**
	 * Sort routes by seats count
	 */
	public void sortRoutesBySeatsCount() {
		routes.sort((first, second) -> {
			return second.getSeatsCount() - first.getSeatsCount();
		});
	}
	
	/**
	 * Sort routes by days of week
	 */
	public void sortRoutesByDaysOfWeek() {
		routes.sort((first, second) -> {
			EnumSet<DayOfWeek> days0 = first.getDaysOfWeek();
			EnumSet<DayOfWeek> days1 = second.getDaysOfWeek();
			/* More days at the top of the list */
			if (days0.size() != days1.size()) {
				return days1.size() - days0.size();
			}
			/* Consider the position of the days in the list */
			int daysCoef0 = 0;
			int daysCoef1 = 0;
			/* Binary representation of the week. Sunday - 7th bit, ... */
			for (DayOfWeek day : days0) {
				daysCoef0 |= 1 << (DayOfWeek.values().length - day.getId() - 1);
			}
			for (DayOfWeek day : days1) {
				daysCoef1 |= 1 << (DayOfWeek.values().length - day.getId() - 1);
			}
			return daysCoef1 - daysCoef0;
		});
	}
	
	/**
	 * Sort routes by route number
	 */
	public void sortRoutesByRouteNumber() {
		routes.sort((first, second) -> {
			return second.getRouteNumber() - first.getRouteNumber();
		});
	}
	
	@Override
	public String toString() {
		StringBuilder msg = new StringBuilder();
		msg.append("TicketOffice { routes {\n");
		for (Route route : routes) {
			msg.append(route.toString()).append('\n');
		}
		msg.append("} }");
		return msg.toString();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof TicketOffice) {
			return ((TicketOffice) obj).routes.equals(routes);
		}
		return false;
	}
}