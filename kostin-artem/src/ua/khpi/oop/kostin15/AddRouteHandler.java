package ua.khpi.oop.kostin15;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;

import ua.khpi.oop.kostin15.dialog.DialogHandler;

/**
 * Dialog-mode handler for the "Add route" option
 * @author Kostin A.S. CS-920b
 */
public class AddRouteHandler extends DialogHandler {
	/** Route to edit and add */
	private Route route;
	
	/**
	 * Constructor without parameters 
	 */
	public AddRouteHandler() {
		route = new Route();
		addOption("Print route", params -> {
			System.out.println(route);
		});
		addOption("Edit route number", params -> {
			editRouteNumberHandler(params);
		});
		addOption("Edit seats count", params -> {
			editSeatsCountHandler(params);
		});
		addOption("Edit days of week", new EditDaysOfWeekHandler());
		addOption("Add to the office", params -> {
			TicketOffice office = (TicketOffice)params.get(1);
			office.addRoute(route);
			System.out.println("Added route: " + route);
		});
	}
	
	@Override
	protected void onExit() {
		route = new Route();
	}
	
	/**
	 * Handler for the "Edit route number" option
	 * @param params Stack with params
	 * @throws IOException
	 */
	private void editRouteNumberHandler(ArrayList<Object> params)
			throws IOException {
		BufferedReader in = (BufferedReader)params.get(0);
		while (true) {
			System.out.println("Enter a new route number:");
			String input = in.readLine();
			if (input == null) {
				return;
			}
			int number = 0;
			try {
				number = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Incorrect number format. Try again");
				continue;
			}
			try {
				route.setRouteNumber(number);
			} catch (IllegalArgumentException e) {
				System.out.println("Incorrect route number. Try again");
				continue;
			}
			
			System.out.println("New route number: " + route.getRouteNumber());
			return;
		}
	}
	
	/**
	 * Handler for the "Edit route number" option
	 * @param params Stack with params
	 * @throws IOException
	 */
	private void editSeatsCountHandler(ArrayList<Object> params)
			throws IOException {
		BufferedReader in = (BufferedReader)params.get(0);
		while (true) {
			System.out.println("Enter a new seats count:");
			String input = in.readLine();
			if (input == null) {
				return;
			}
			int count = 0;
			try {
				count = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Incorrect number format. Try again");
				continue;
			}
			try {
				route.setSeatsCount(count);
			} catch (IllegalArgumentException e) {
				System.out.println("Incorrect route number. Try again");
				continue;
			}
			System.out.println("New seats count: " + route.getSeatsCount());
			return;
		}
	}
	
	/**
	 * Dialog-mode handler for the "Edit days of week" option
	 * @author Kostin A.S. CS-920b
	 */
	private class EditDaysOfWeekHandler extends DaysOfWeekHandler {
		@Override
		protected void onExit() {
			/* Apply on exit */
			EnumSet<DayOfWeek> set = selectedToEnum();
			route.setDaysOfWeek(set);
			System.out.println("New days of week: " + set);
			selectedDays = 0;
		}
	}
}
