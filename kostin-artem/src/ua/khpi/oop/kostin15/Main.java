package ua.khpi.oop.kostin15;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.EnumSet;

import ua.khpi.oop.kostin15.dialog.DialogHandler;

/**
 * Main task:
 * - Develop a console program to implement the task of data processing
 *   according to the application area.
 * - Use containers (collections) and algorithms from the Java Collections
 *   Framework to host and process data.
 * - Provide processing of the collection of objects: adding, deleting,
 *   searching, sorting according to the section Applied tasks l.w. №10.
 * - Provide for the possibility of long-term data storage:
 *   1) using standard serialization;
 *   2) without using the serialization protocol.
 * - Demonstrate the developed functionality in dialog and automatic modes as a
 *   result of processing command line parameters.
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		if (!(args.length > 0 && args[0].equals("auto"))) {
			runDialogLoop();
			return;
		}
		// Automatic mode
		TicketOffice office = new TicketOffice();
		office.addRoute(new Route(300,
				EnumSet.of(DayOfWeek.SUNDAY, DayOfWeek.MONDAY,
						DayOfWeek.TUESTAY), 12));
		office.addRoute(new Route(150,
				EnumSet.of(DayOfWeek.SUNDAY, DayOfWeek.TUESTAY,
						DayOfWeek.THURSDAY), 18));
		office.addRoute(new Route(400,
				EnumSet.of(DayOfWeek.FRIDAY), 3));
		office.addRoute(new Route(130,
				EnumSet.of(DayOfWeek.THURSDAY, DayOfWeek.SATURDAY), 5));
		office.addRoute(new Route(320,
				EnumSet.of(DayOfWeek.SUNDAY, DayOfWeek.TUESTAY,
						DayOfWeek.WEDNESDAY), 11));
		System.out.println(office);
		office.sortRoutesByRouteNumber();
		System.out.println(office);
		office.sortRoutesBySeatsCount();
		System.out.println(office);
		office.sortRoutesByDaysOfWeek();
		System.out.println(office);
		// Serialization
		if (!serializeTicketOffice(office, "./data.dat")) {
			System.out.println("Can't serialize");
			return;
		}
		System.out.println("Serialized");
		TicketOffice office1 = deserializeTicketOffice("./data.dat");
		if (office1 == null) {
			System.out.println("Can't deserialize");
			return;
		}
		System.out.println("Deserialized");
		System.out.println(office1);
		// XML
		if (!encodeTicketOffice(office, "./data.xml")) {
			System.out.println("Can't serialize");
			return;
		}
		System.out.println("Bean Serialized");
		TicketOffice office2 = decodeTicketOffice("./data.xml");
		if (office2 == null) {
			System.out.println("Can't deserialize bean");
			return;
		}
		System.out.println("Bean deserialized");
		System.out.println(office1);
	}
	
	/**
	 * Run main dialog loop
	 */
	private static void runDialogLoop() {
		/* Office to process */
		TicketOffice office = new TicketOffice();
		/* Dialog handler maybe? */
		DialogHandler handler = new DialogHandler();
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in))) {
			/* Stack of parameters */
			ArrayList<Object> stack = new ArrayList<>();
			/* 0 - reader */
			stack.add(reader);
			/* 1 - office */
			stack.add(office);
			/* Add dialog options and handlers for them */
			handler.addOption("Print office", params -> {
				printOfficeHandler(params);
				});
			handler.addOption("Add route", 
					new AddRouteHandler());
			handler.addOption("Remove route", params -> {
				removeRouteHandler(params);
				});
			handler.addOption("Find route by days of week",
					new FindRoutesHandler());
			handler.addOption("Sort routes by route number", params -> {
				((TicketOffice)params.get(1)).sortRoutesByRouteNumber();
			});
			handler.addOption("Sort routes by seats count", params -> {
				((TicketOffice)params.get(1)).sortRoutesBySeatsCount();
			});
			handler.addOption("Sort routes by days of week", params -> {
				((TicketOffice)params.get(1)).sortRoutesByDaysOfWeek();
			});
			handler.addOption("Serialize office", params -> {
				serializeHandler(params);
				});
			handler.addOption("Serialize office to XML", params -> {
				serializeXmlHandler(params);
			});
			handler.addOption("Deserialize office", params -> {
				deserializeHandler(params);
			});
			handler.addOption("Deserialize office from XML", params -> {
				deserializeXmlHandler(params);
			});
			/* Start the root dialog processing */
			handler.handleOption(stack);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Handle "Print office" option
	 * @param params Stack of parameters
	 * @throws IOException
	 */
	private static void printOfficeHandler(ArrayList<Object> params)
			throws IOException {
		/* Retrieve data from stack */
		TicketOffice office = (TicketOffice)params.get(1);
		System.out.println(office.toString());
	}
	
	/**
	 * Handle "Remove route" option
	 * @param params Stack of parameters
	 * @throws IOException
	 */
	private static void removeRouteHandler(ArrayList<Object> params)
			throws IOException {
		/* Retrieve data from stack */
		TicketOffice office = (TicketOffice)params.get(1);
		if (office.getRoutes().size() == 0) {
			System.out.println("There is no routes to delete");
			return;
		}
		BufferedReader in = (BufferedReader)params.get(0);
		while (true) {
			System.out.println("Enter the index of route you want to delete:");
			String input = in.readLine();
			if (input == null) {
				return;
			}
			int index = 0;
			try {
				index = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Incorrect number format. Try again");
				continue;
			}
			if (index < 0 || index >= office.getRoutes().size()) {
				System.out.println("Index out of bounds. Try again");
				continue;
			}
			office.removeRoute(index);
			System.out.println("Route was removed");
		}
	}
	
	/**
	 * Handle "Serialize" option
	 * @param params Stack of parameters
	 * @throws IOException
	 */
	private static void serializeHandler(ArrayList<Object> params)
			throws IOException {
		/* Retrieve data from stack */
		BufferedReader in = (BufferedReader)params.get(0);
		TicketOffice office = (TicketOffice)params.get(1);
		System.out.println("Enter the path to serialize office:");
		String input = in.readLine();
		if (input == null) {
			return;
		}
		if (!serializeTicketOffice(office, input)) {
			System.out.println("Can't serialize office");
		}
		System.out.println("Serialized");
	}
	
	/**
	 * Handle "Serialize to XML" option
	 * @param params Stack of parameters
	 * @throws IOException
	 */
	private static void serializeXmlHandler(ArrayList<Object> params)
			throws IOException {
		/* Retrieve data from stack */
		BufferedReader in = (BufferedReader)params.get(0);
		TicketOffice office = (TicketOffice)params.get(1);
		System.out.println("Enter the path to serialize office:");
		String input = in.readLine();
		if (input == null) {
			return;
		}
		if (!encodeTicketOffice(office, input)) {
			System.out.println("Can't serialize office");
		}
		System.out.println("Serialized");
	}
	
	/**
	 * Handle "Deserialize" option
	 * @param params Stack of parameters
	 * @throws IOException
	 */
	private static void deserializeHandler(ArrayList<Object> params)
			throws IOException {
		/* Retrieve data from stack */
		BufferedReader in = (BufferedReader)params.get(0);
		System.out.println("Enter the path of the serialized office:");
		String input = in.readLine();
		if (input == null) {
			return;
		}
		TicketOffice office = deserializeTicketOffice(input);
		if (office == null) {
			System.out.println("Can't deserialize office");
			return;
		}
		params.set(1, office);
		System.out.println("Deserialized");
	}
	
	/**
	 * Handle "Deserialize from XML" option
	 * @param params Stack of parameters
	 * @throws IOException
	 */
	private static void deserializeXmlHandler(ArrayList<Object> params)
			throws IOException {
		/* Retrieve data from stack */
		BufferedReader in = (BufferedReader)params.get(0);
		System.out.println("Enter the path of the office's XML:");
		String input = in.readLine();
		if (input == null) {
			return;
		}
		TicketOffice office = decodeTicketOffice(input);
		if (office == null) {
			System.out.println("Can't deserialize office");
			return;
		}
		params.set(1, office);
		System.out.println("Deserialized");
	}
	
	/**
	 * Serialize office to file
	 * @param office Office
	 * @param path Path to a file
	 * @return Status of the operation. True if it succeeded
	 */
	private static boolean serializeTicketOffice(TicketOffice office,
			String path) {
		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(path))) {
			oos.writeObject(office);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Deserialize office from file
	 * @param path Path to a file
	 * @return Deserialized office
	 */
	private static TicketOffice deserializeTicketOffice(String path) {
		TicketOffice result = null;
		try (ObjectInputStream oos = new ObjectInputStream(
				new FileInputStream(path))) {
			result = (TicketOffice) oos.readObject();
		} catch (IOException | ClassNotFoundException e) {
			return null;
		}
		return result;
	}
	
	/**
	 * Serialize office to file using LTP model
	 * @param office Office
	 * @param path Path to a file
	 * @return Status of the operation. True if it succeeded
	 */
	private static boolean encodeTicketOffice(TicketOffice office,
			String path) {
		try (XMLEncoder encoder = new XMLEncoder(
				new BufferedOutputStream(
						new FileOutputStream(path)))) {
			encoder.writeObject(office);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Deserialize office from file using LTP model
	 * @param path Path to a file
	 * @return Deserialized office
	 */
	private static TicketOffice decodeTicketOffice(String path) {
		TicketOffice result = null;
		try (XMLDecoder decoder = new XMLDecoder(
			    new BufferedInputStream(
			    	    new FileInputStream(path)))) {
			result = (TicketOffice) decoder.readObject();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
}
