package ua.khpi.oop.kostin15;

import java.io.Serializable;
import java.util.Objects;

/**
 * Enum that represents days of week
 * @author Kostin A.S. CS-920b
 */
public enum DayOfWeek implements Serializable {
	/** Sunday */
	SUNDAY("Sunday", 0),
	/** Monday */
	MONDAY("Monday", 1),
	/** Tuesday */
	TUESTAY("Tuesday", 2),
	/** Wednesday */
	WEDNESDAY("Wednesday", 3),
	/** Thursday */
	THURSDAY("Thursday", 4),
	/** Friday */
	FRIDAY("Friday", 5),
	/** Saturday */
	SATURDAY("Saturday", 6);
	
	/** Name of the day of week */
	private final String name;
	private final int id;
	
	/**
	 * Constructor with parameters
	 * @param name Name of the day of week
	 */
	DayOfWeek(String name, int id) {
		Objects.requireNonNull(name);
		this.name = name;
		this.id = id;
	}
	
	/**
	 * @return Day's id
	 */
	public int getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
