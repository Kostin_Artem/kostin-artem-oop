package ua.khpi.oop.kostin15;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Objects;

/**
 * Class that represents route data
 * @author Kostin A.S. CS-920b
 */
public final class Route implements Serializable {
	private static final long serialVersionUID = 2749651374512156304L;
	/** Array of nodes of the route */
	private ArrayList<RouteNode> nodes;
	/** Total number of free seats */
	private int seatsCount;
	/** Days on which the route runs */
	private EnumSet<DayOfWeek> daysOfWeek;
	/** Number of the route */
	private int routeNumber;

	/** Constructor without parameters */
	public Route() {
		nodes = new ArrayList<>();
		daysOfWeek = EnumSet.noneOf(DayOfWeek.class);
	}
	
	/**
	 * Constructor with parameters
	 * @param seats Total number of free seats
	 * @param days Days on which the route runs
	 * @param routeNum Number of the route
	 */
	public Route(int seats, EnumSet<DayOfWeek> days, int routeNum) {
		Objects.requireNonNull(days);
		nodes = new ArrayList<>();
		seatsCount = seats;
		daysOfWeek = days.clone();
		routeNumber = routeNum;
	}
	
	/**
	 * Copy constructor
	 * @param route Route for copying
	 */
	@SuppressWarnings("unchecked")
	public Route(Route route) {
		Objects.requireNonNull(route);
		nodes = (ArrayList<RouteNode>) route.nodes.clone();
		seatsCount = route.seatsCount;
		daysOfWeek = EnumSet.copyOf(route.daysOfWeek);
		routeNumber = route.routeNumber;
	}
	
	/**
	 * Adds a RouteNode to the route
	 * @param node RouteNode to add
	 */
	public void addNode(RouteNode node) {
		Objects.requireNonNull(node);
		nodes.add(node);
	}
	
	/**
	 * @return Number of the route
	 */
	public int getRouteNumber() {
		return routeNumber;
	}
	
	/**
	 * @return Seats count
	 */
	public int getSeatsCount() {
		return seatsCount;
	}
	
	/**
	 * @return Copy of the nodes list
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<RouteNode> getNodes() {
		return (ArrayList<RouteNode>) nodes.clone();
	}
	
	/**
	 * @return Days of week
	 */
	public EnumSet<DayOfWeek> getDaysOfWeek() {
		return daysOfWeek;
	}
	
	/**
	 * Sets new route number
	 * @param routeNumber
	 */
	public void setRouteNumber(int routeNumber) {
		if (routeNumber < 0) {
			throw new IllegalArgumentException();
		}
		this.routeNumber = routeNumber;
	}
	
	/**
	 * Sets new seats count
	 * @param seatsCount Seats count
	 */
	public void setSeatsCount(int seatsCount) {
		if (seatsCount < 0) {
			throw new IllegalArgumentException();
		}
		this.seatsCount = seatsCount;
	}
	
	/**
	 * Sets days on which the route runs
	 * @param daysOfWeek Days on which the route runs
	 */
	public void setDaysOfWeek(EnumSet<DayOfWeek> daysOfWeek) {
		Objects.requireNonNull(daysOfWeek);
		this.daysOfWeek = daysOfWeek.clone();
	}
	
	/**
	 * Sets new array of nodes
	 * @param nodes Array of nodes
	 */
	@SuppressWarnings("unchecked")
	public void setNodes(ArrayList<RouteNode> nodes) {
		Objects.requireNonNull(nodes);
		this.nodes = (ArrayList<RouteNode>) nodes.clone();
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Route { ").append(nodes.toString()).append(", ")
		   .append(routeNumber).append(", ")
		   .append(Arrays.toString(daysOfWeek.toArray())).append(", ")
		   .append(seatsCount).append(" }");
		return str.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof Route) {
			Route route = ((Route) obj);
			return route.routeNumber == routeNumber
					&& route.seatsCount == seatsCount
					&& route.nodes.equals(nodes)
					&& route.daysOfWeek.equals(daysOfWeek);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
