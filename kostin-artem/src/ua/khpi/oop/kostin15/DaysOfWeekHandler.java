package ua.khpi.oop.kostin15;

import java.util.ArrayList;
import java.util.EnumSet;

import ua.khpi.oop.kostin15.dialog.DialogHandler;

/**
 * Dialog-mode handler which provides processing for selecting the days of week
 * @author Kostin A.S. CS-920b
 */
public class DaysOfWeekHandler extends DialogHandler {
	/** Bit representation of selected days. Sunday - 7th bit */
	protected int selectedDays;
	
	public DaysOfWeekHandler() {
		selectedDays = 0;
		/* Days toggle options */
		addOption("Print Selected", params -> {
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[0].toString(), params -> {
			selectedDays ^= 1 << 6;
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[1].toString(), params -> {
			selectedDays ^= 1 << 5;
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[2].toString(), params -> {
			selectedDays ^= 1 << 4;
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[3].toString(), params -> {
			selectedDays ^= 1 << 3;
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[4].toString(), params -> {
			selectedDays ^= 1 << 2;
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[5].toString(), params -> {
			selectedDays ^= 1 << 1;
			printSelectedDays();
		});
		addOption("Toggle " + DayOfWeek.values()[6].toString(), params -> {
			selectedDays ^= 1;
			printSelectedDays();
		});
	}
	
	/**
	 * Сonvert bit representation of the selected days to EnumSet
	 * @return
	 */
	protected EnumSet<DayOfWeek> selectedToEnum() {
		/* List of days */
		ArrayList<DayOfWeek> days = new ArrayList<>();
		int daysCount = DayOfWeek.values().length;
		for (int i = daysCount - 1; i >= 0; --i) {
			if (((selectedDays >> i) & 1) == 1) {
				days.add(DayOfWeek.values()[daysCount - i - 1]);
			}
		}
		return EnumSet.copyOf(days);
	}
	
	/**
	 * Print selected days
	 */
	private void printSelectedDays() {
		StringBuffer buf = new StringBuffer();
		buf.append("Selected days: [");
		int daysCount = DayOfWeek.values().length;
		for (int i = daysCount - 1; i >= 0; --i) {
			if (((selectedDays >> i) & 1) == 1) {
				buf.append(DayOfWeek.values()[daysCount - i - 1].toString());
				/* Print comma if there are more days in the list */
				/* Mask processed days */
				if ((selectedDays & ~(~0 << i)) != 0) {
					buf.append(", ");
				}
			}
		}
		buf.append("]\n");
		System.out.print(buf.toString());
	}
}
