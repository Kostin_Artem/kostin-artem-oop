package ua.khpi.oop.kostin09;

/**
 * Doubly linked list node
 * @author Kostin A.S. CS-920b
 * @param <T> Data type
 */
public final class MyNode<T> {
	/** Current element. May be null */
	private T element;
	/** Next node */
	private MyNode<T> prev;
	/** Prev node */
	private MyNode<T> next;
	
	/**
	 * Constructor
	 * @param element Current element
	 * @param prev Previous node
	 * @param next Next node
	 */
	public MyNode(T element, MyNode<T> prev, MyNode<T> next) {
		this.element = element;
		this.prev = prev;
		this.next = next;
	}

	/** @return Current element */
	public T getElement() {
		return element;
	}

	/**
	 * Sets current element
	 * @param element Element to set
	 */
	public void setElement(T element) {
		this.element = element;
	}

	/**
	 * Get previous node
	 * @return Previous node
	 */
	public MyNode<T> getPrev() {
		return prev;
	}

	/**
	 * Sets previous node
	 * @param prev Previous node
	 */
	public void setPrev(MyNode<T> prev) {
		this.prev = prev;
	}

	/**
	 * @return Next node
	 */
	public MyNode<T> getNext() {
		return next;
	}

	/**
	 * Sets next mode
	 * @param next Next node
	 */
	public void setNext(MyNode<T> next) {
		this.next = next;
	}
	
	/**
	 * Clears element and links to the nodes
	 */
	public void clear() {
		element = null;
		prev = null;
		next = null;
	}
}
