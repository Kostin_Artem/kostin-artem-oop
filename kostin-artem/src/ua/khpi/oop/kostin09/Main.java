package ua.khpi.oop.kostin09;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.EnumSet;

/**
 * Main task:
 * - Create own generic container based on linked lists to implement
 *   a collection of domain-objects of laboratory work №7
 * - For developed container classes, provide the ability to use their objects
 *   in the foreach loop as a data source
 * - Provide the ability to save and restore the collection of objects:
 *   - Using standard serialization
 *   - Without using the serialization protocol
 * - Demonstrate developed functionality:
 *   - Create a container
 *   - Add items
 *   - Delete items
 *   - Clean a container
 *   - Convert to an array
 *   - Convert to a string
 *   - Check for items
 * - The use of containers from the Java Collections Framework is prohibited 
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	/** Path to the serialization file */
	private static final String S_FILE = "mycontainer.ser";
	/** Path to the externalization file */
	private static final String EXT_FILE = "mycontainer.ext";
	
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		testSerialization();
		System.out.println();
		createDomains();
	}
	
	/**
	 * Demonstration of using own container with domain objects
	 */
	private static void createDomains() {
		System.out.println("Main.createDomains()");
		/* Ticket office for demonstration */
		TicketOffice office = new TicketOffice();
		/* Route to add */
		Route temp = new Route(250, EnumSet.of(DayOfWeek.MONDAY), 1);
		/* Stream to print cyrillic */
		PrintStream out = null;
		try {
			out = new PrintStream(System.out, true, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		temp.addNode(new RouteNode("Харьков", null, new Time(14, 30, 0), 250));
		temp.addNode(new RouteNode("Краков", new Time(20, 14, 0),
				new Time(20, 45, 30), 120));
		temp.addNode(new RouteNode("Тарков", new Time(8, 10, 45), null, 0));
		office.addRoute(temp);
		
		temp = new Route(130, EnumSet.of(DayOfWeek.MONDAY,
				DayOfWeek.TUESTAY, DayOfWeek.WEDNESDAY,
				DayOfWeek.SATURDAY), 2);
		temp.addNode(new RouteNode("Харьков", null, new Time(7, 30, 0), 130));
		temp.addNode(new RouteNode("Хогвартс", new Time(8, 30, 0), null, 0));
		office.addRoute(temp);
		
		out.println(office.toString());
	}
	
	/**
	 * Demonstration of using serialization and externalization mechanisms
	 */
	@SuppressWarnings("unchecked")
	private static void testSerialization() {
		System.out.println("Main.testSerialization()");
		MyContainer<String> cont1 = new MyContainer<>();
		MyContainer<String> cont2 = new MyContainer<>();
		
		cont1.pushBack("Lol");
		cont1.pushBack("Kkekoko");
		cont1.pushBack("Chebuwreck hashsh");
		// Demonstration of using toArray method
		Object[] arr = cont1.toArray();
		for (Object a : arr) {
			System.out.println(a);			
		}
		
		// Demonstration of using foreach loop
		System.out.println(cont1.toString());
		for (String s : cont1) {
			System.out.println(s);
		}
		
		System.out.println("Serializing the first container");
		try(ObjectOutputStream stream = new ObjectOutputStream(
				new FileOutputStream(new File(S_FILE)))) {
			stream.writeObject(cont1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Deserializing...");
		try(ObjectInputStream stream = new ObjectInputStream(
				new FileInputStream(new File(S_FILE)))) {
			cont2 = (MyContainer<String>) stream.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		// Demonstration of using clear method
		System.out.println(cont2);
		cont2.clear();
		
		System.out.println("Externalizing the first container");
		try(ObjectOutputStream stream = new ObjectOutputStream(
				new FileOutputStream(new File(EXT_FILE)))) {
			cont1.writeExternal(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Deexternalizing...");
		try(ObjectInputStream stream = new ObjectInputStream(
				new FileInputStream(new File(EXT_FILE)))) {
			cont2.readExternal(stream);;
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		System.out.println(cont2);
	}
}
