package ua.khpi.oop.kostin09;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class that represents ticket office data
 * @author Kostin A.S. CS-920b
 */
public final class TicketOffice implements Serializable {
	/** Serial ID for serialization */
	private static final long serialVersionUID = 1L;
	/** Array of available routes */
	private MyContainer<Route> routes;
	
	/** Constructor without parameters */
	public TicketOffice() {
		routes = new MyContainer<>();
	}
	
	/**
	 * Add new route to the array
	 * @param route New route
	 */
	public void addRoute(Route route) {
		Objects.requireNonNull(route);
		routes.pushBack(route);
	}
	
	@Override
	public String toString() {
		StringBuilder msg = new StringBuilder();
		msg.append("TicketOffice { routes {\n");
		for (Route route : routes) {
			msg.append(route.toString()).append('\n');
		}
		msg.append("} }");
		return msg.toString();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
}
