package ua.khpi.oop.kostin09;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Objects;

/**
 * Class that represents route data
 * @author Kostin A.S. CS-920b
 */
public final class Route implements Serializable {
	/** Serial ID for serialization */
	private static final long serialVersionUID = 1L;
	
	/** Array of nodes of the route */
	private ArrayList<RouteNode> nodes;
	/** Total number of free seats */
	private int seatsCount;
	/** Days on which the route runs */
	private EnumSet<DayOfWeek> daysOfWeek;
	/** Number of the route */
	private int routeNumber;

	/**
	 * Constructor with parameters
	 * @param seats Total number of free seats
	 * @param days Days on which the route runs
	 * @param routeNum Number of the route
	 */
	public Route(int seats, EnumSet<DayOfWeek> days, int routeNum) {
		nodes = new ArrayList<>();
		seatsCount = seats;
		daysOfWeek = days.clone(); // EnumSet is not final so is not immutable
		routeNumber = routeNum;
	}
	
	/**
	 * Copy constructor
	 * @param route Route for copying
	 */
	@SuppressWarnings("unchecked")
	public Route(Route route) {
		nodes = (ArrayList<RouteNode>) route.nodes.clone();
		seatsCount = route.seatsCount;
		daysOfWeek = EnumSet.copyOf(route.daysOfWeek);
		routeNumber = route.routeNumber;
	}
	
	/**
	 * Adds a RouteNode to the route
	 * @param node RouteNode to add
	 */
	public void addNode(RouteNode node) {
		Objects.requireNonNull(node);
		nodes.add(node);
	}
	
	/**
	 * @return Number of the route
	 */
	public int getRouteNumber() {
		return routeNumber;
	}
	
	public EnumSet<DayOfWeek> getDaysOfWeek() {
		return daysOfWeek;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Route { ").append(nodes.toString()).append(", ")
		   .append(seatsCount).append(", ")
		   .append(Arrays.toString(daysOfWeek.toArray())).append(", ")
		   .append(routeNumber).append(" }");
		return str.toString();
	}
}
