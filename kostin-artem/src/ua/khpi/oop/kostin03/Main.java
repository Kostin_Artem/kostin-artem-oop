package ua.khpi.oop.kostin03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Main task:
 * - Use latin charset
 * - Use StringBuffer or StringBuilder
 * - Create own helper class and use static methods to process data
 * - Don't use classes of package java.util.regex or String.matches etc..
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		/* Map with words */
		HashMap<String, Integer> wordsCount = new HashMap<>();
		/* Console reader */
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));
		String line = null;
		
		try {
			line = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (line == null) {
			return;
		}
		
		/* StringTokenizer is not a part of RegEx */
		StringTokenizer tokenizer = new StringTokenizer(line);
		/* Getting words */
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			wordsCount.put(token, wordsCount.getOrDefault(token, 0) + 1);
		}
		
		IoHelper.printAsTable(
				new Pair<String, String>("Word name", "Count"),
				wordsCount,
				System.out);
	}
}
