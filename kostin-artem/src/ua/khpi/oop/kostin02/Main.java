package ua.khpi.oop.kostin02;

import java.util.Random;

/**
 * Main task:
 * - Use Random to generate input data to test the solution
 * - Output the results to the console
 * - Do not use the String class or arrays
 * - Find the sum of all even digits and the sum of all odd digits in the
 *   decimal notation of an 8-digit integer
 * 
 * @date 01.11.2021
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		Main.performTask();
	}
	
	/**
	 * Demonstration of the solution
	 */
	public static void performTask() {
		Random random = new Random();
		
		for (int i = 0; i < 15; ++i) {
			long number = random.nextLong() % 100_000_000L;
			if (number == 0) {
				number = 1;
			}
			while (number / 10_000_000L == 0) {
				number *= 10;
			}
			BytePair result = Main.parseDigitsParity(number);
			
			System.out.format("%3d: Number: %9d, Odd: %3d, Even: %3d%n", i,
							  number, result.second, result.first);
		}
	}
	
	/**
	 * Add digit to a corresponding variable
	 * first  - even
	 * second - odd
	 * @param result Pair of the sums of odd and even digits
	 * @param digit A digit to add
	 */
	public static void addToCorresponding(BytePair result, byte digit) {
		if ((digit & 1) == 0) {
			result.first += digit < 0 ? -digit : digit;
		} else {
			result.second += digit < 0 ? -digit : digit;
		}
	}
	
	/**
	 * Calculates the sums of odd and even digits in a number
	 * @param number 8-digits number to parse
	 * @return Sums of odd and even digits in a number
	 */
	public static BytePair parseDigitsParity(long number) {
		// first == even, second == odd
		BytePair result = new BytePair();
		byte last = (byte) (number % 10);
		
		// Correct inversion of the number to obtain only positive values
		if (number == Long.MIN_VALUE) {
			number /= -10; // Inverse number
		} else {
			number /= 10;
		}
		Main.addToCorresponding(result, last);
		
		while(number != 0) {
			last = (byte) (number % 10);
			number /= 10;
			
			Main.addToCorresponding(result, last);
		}
		
		return result;
	}
}
