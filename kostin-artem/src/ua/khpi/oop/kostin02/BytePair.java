package ua.khpi.oop.kostin02;

/**
 * Pair of two byte variables
 * @author Kostin A.S. CIT-120b
 */
public class BytePair {
	public byte first;
	public byte second;
	
	/**
	 * Default constructor
	 */
	public BytePair() {
		first = 0;
		second = 0;
	}
}
