package ua.khpi.oop.kostin07;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.EnumSet;

/**
 * Main task:
 * - Using object-oriented analysis, implement classes to represent
 *   the entities according to the applied problem - domain-objects.
 * - Ensure and demonstrate the correct introduction and display of Cyrillic.
 * - Demonstrate the ability to manage an array of domain objects. 
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		/* Ticket office for demonstration */
		TicketOffice office = new TicketOffice();
		/* Route to add */
		Route temp = new Route(250, EnumSet.of(DayOfWeek.MONDAY), 1);
		/* Stream to print cyrillic */
		PrintStream out = null;
		try {
			out = new PrintStream(System.out, true, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		temp.addNode(new RouteNode("Харьков", null, new Time(14, 30, 0), 250));
		temp.addNode(new RouteNode("Краков", new Time(20, 14, 0),
				new Time(20, 45, 30), 120));
		temp.addNode(new RouteNode("Тарков", new Time(8, 10, 45), null, 0));
		office.addRoute(temp);
		
		temp = new Route(130, EnumSet.of(DayOfWeek.MONDAY,
				DayOfWeek.TUESTAY, DayOfWeek.WEDNESDAY,
				DayOfWeek.SATURDAY), 2);
		temp.addNode(new RouteNode("Харьков", null, new Time(7, 30, 0), 130));
		temp.addNode(new RouteNode("Хогвартс", new Time(8, 30, 0), null, 0));
		office.addRoute(temp);
		
		out.println(office.toString());
	}
}
