package ua.khpi.oop.kostin01;

/**
 * Main task:
 * - Choose the type of variables and set the initial values ​​with constants
 *   and literals:
 *   - A number that corresponds to the number of the record book using
 *     a hexadecimal literal;
 *   - A number corresponding to a mobile phone number (starting with 380 ...)
 *     using a decimal literal;
 *   - A number consisting of the last two non-zero digits of the mobile phone
 *     number using a binary literal;
 *   - A number consisting of the last four non-zero digits of the mobile phone
 *     number using an octal literal;
 *   - Determine the value of the remainder of the division by 26 increased by
 *     one reduced by one student number in the group journal;
 *   - Uppercase English alphabet character, the number of which corresponds to
 *     the previously found value.
 * - Using the decimal notation of the integer value of each variable, find
 *   and count the number of even and odd digits.
 * - Using the binary record of the integer value of each variable, count the
 *   number of units. 
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Calculates odd and even digits count in a number
	 * @param number Number to calculate
	 * @return Odd and even digits count in a number
	 */
	public static int[] getOddEvenCount(long number) {
		// 0 - even
		// 1 - odd
		int result[] = new int[] {0, 0};
		do {
			++result[(int) (number % 2)];
			number /= 10;
		} while (number != 0);
		
		return result;
	}
	
	/**
	 * Calculates ones count in binary representation
	 * @param number Number to calculate
	 * @return Ones count
	 */
	public static int getOnesCount(long number) {
		int result = 0;
		do {
			if ((number & 1) == 1) {
				++result;
			}
			number /= 2;
		} while (number != 0);
		
		return result;
	}
	
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String args[]) {
		final int rbNumber = 125165;
		final long phNumber = 380994206969L;
		final int last2 = 0b1000101;
		final int last4 = 015471;
		final int studentNum = 9;
		final int result = (studentNum - 1) % 26 + 1;
		final char symbol = 'A' + result;
		
		// Calculate odd and even digits count in variables
		final int varCount = 7;
		int[][] numberInfo = new int[varCount][2];
		numberInfo[0] = getOddEvenCount(rbNumber);
		numberInfo[1] = getOddEvenCount(phNumber);
		numberInfo[2] = getOddEvenCount(last2);
		numberInfo[3] = getOddEvenCount(last4);
		numberInfo[4] = getOddEvenCount(studentNum);
		numberInfo[5] = getOddEvenCount(result);
		numberInfo[6] = getOddEvenCount(symbol);
		
		// Calculate ones count in binary representation
		int[] numberInfoBin = new int[varCount];
		numberInfoBin[0] = getOnesCount(rbNumber);
		numberInfoBin[1] = getOnesCount(phNumber);
		numberInfoBin[2] = getOnesCount(last2);
		numberInfoBin[3] = getOnesCount(last4);
		numberInfoBin[4] = getOnesCount(studentNum);
		numberInfoBin[5] = getOnesCount(result);
		numberInfoBin[6] = getOnesCount(symbol);
	}
}
