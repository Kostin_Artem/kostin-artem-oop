package ua.khpi.oop.kostin06;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Container class for the task
 * @author Kostin A.S. CIT-120b
 */
public final class MyContainer implements Iterable<String>, Serializable {
	private static final long serialVersionUID = -145022113295591185L;
	/** Default capacity of the container */
	private static final int DEFAULT_CAPACITY = 4;
	
	/** Array with data */
	private String[] array;
	/** Max size of the array */
	private int capacity;
	/** Current size of the array */
	private int size;
	
	/**
	 * Constructor with parameters
	 * @param capacity Initial capacity
	 */
	public MyContainer(int capacity) {
		if (capacity <= 0) {
			throw new NegativeArraySizeException();
		}
		
		this.capacity = capacity;
		size = 0;
		array = new String[capacity];
	}
	
	/**
	 * Default constructor
	 */
	public MyContainer() {
		this(DEFAULT_CAPACITY);
	}
	
	/**
	 * Adds string to the array
	 * @param string String to add
	 */
	public void add(String string) {
		if (string == null) {
			return;
		} else if (size < capacity) {
			array[size] = string;
			++size;
		/* Can't make the array bigger */
		} else if(size == Integer.MAX_VALUE) {
			return;
		} else {
			long newCapacity = capacity * 2L;
			capacity = (newCapacity > Integer.MAX_VALUE)
					   ? Integer.MAX_VALUE : (int)newCapacity;
			array = ArrayHelper.copyOf(array, capacity);
			
			array[size] = string;
			++size;
		}
	}
	
	/**
	 * Removes first entry of the string from the array
	 * @param string String to remove
	 * @return If the string was removed
	 */
	public boolean remove(String string) {
		int index;
		if (size == 0 || string == null) {
			return false;
		}
		
		for (index = 0; index < size; ++index) {
			if (array[index].equals(string)) {
				break;
			}
		}
		if (index == size) {
			return false;
		}
		
		while (index < size - 1) {
			array[index] = array[index + 1];
			++index;
		}
		array[index] = null;
		--size;
		return true;
	}
	
	/**
	 * Clears the array
	 */
	public void clear() {
		if (size != 0) {
			array = new String[capacity];
			size = 0;
		}
	}
	
	/**
	 * @return Current size of the array
	 */
	public int size() {
		return size;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append('[');
		for (int i = 0; i < size; ++i) {
			str.append(array[i]);
			if (i != size - 1) {
				str.append(", ");
			}
		}
		str.append(']');
		return str.toString();
	}
	
	/**
	 * @return Array representation of the container (Copy)
	 */
	public Object[] toArray() {
		return ArrayHelper.copyOf(array, size);
	}
	
	/**
	 * Checks if there is the string in the array
	 * @param string String to find
	 * @return if there is the string in the array
	 */
	public boolean contains(String string) {
		if (string == null) {
			return false;
		}
		
		for (String it : this) {
			if (it.equals(string)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if there are the string in the array
	 * @param container Container with string to find
	 * @return if there are all the string in the array
	 */
	public boolean containsAll(MyContainer container) {
		if (container == null || container.size() == 0) {
			return false;
		}
		
		for (String it : container) {
			if (!contains(it)) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof MyContainer)) {
            return false;
        }
        
        {
        	MyContainer other = (MyContainer) obj;
        	if (size != other.size) {
        		return false;
        	}
        	for (int i = 0; i < size; ++i) {
        		if (!array[i].equals(other.array[i])) {
        			return false;
        		}
        	}
        	return true;
        }
	}
	
	/**
	 * Finds string in the array
	 * @param obj String to find
	 * @return String's index or -1 if it wasn't found
	 */
	public int find(String obj) {
		if (obj == null) {
			return -1;
		}
		
		int i = 0;
		while (i < size) {
			if (obj.equals(array[i])) {
				return i;
			}
			++i;
		}
		return -1;
	}
	
	/**
	 * Sorts the array in ascending order
	 */
	public void sort() {
		for (int i = 0; i < size - 1; ++i) {
			int min = i;
			String temp = null;
			
			for (int j = size - 1; j > i; --j) {
				if (array[min].compareTo(array[j]) > 0) {
					min = j;
				}
			}
			
			temp = array[min];
			array[min] = array[i];
			array[i] = temp;
		}
	}
	
	@Override
	public Iterator<String> iterator() {
		return new MyIterator();
	}
	
	/**
	 * MyContainer iterator
	 * @author Kostin A.S. CIT-120b
	 */
	private class MyIterator implements Iterator<String> {
		/** Current element pointer */
		int cursor = 0;
		
		@Override
		public boolean hasNext() {
			return size != 0 && cursor < size;
		}

		@Override
		public String next() {
			if (cursor >= size || size == 0) {
				throw new NoSuchElementException();
			}
			return array[cursor++];
		}
		
		@Override
		public void remove() {
			if (cursor >= size || size == 0) {
				return;
			}
			
			{
				int index = cursor;
				while (index < size - 1) {
					array[index] = array[index + 1];
					++index;
				}
				/* Free the element and move the cursor back */
				array[index] = null;
				--size;
				if (cursor != 0) {
					--cursor;					
				}
			}
		}
	}
}
