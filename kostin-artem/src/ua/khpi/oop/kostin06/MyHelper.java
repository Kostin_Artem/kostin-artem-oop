package ua.khpi.oop.kostin06;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

/**
 * Helper class with IO functions
 * @author Kostin A.S. CIT-120b
 */
public class MyHelper {
	/**
	 * Prints map to an output stream as a table
	 * @param <T> Type of the second object in the map
	 * @param names Title of the table
	 * @param map Map to be printed
	 * @param out Stream for printing
	 */
	public static <T> void printAsTable(Pair<String, String> names,
			Map<String, T> map, PrintStream out) {
		/* Length of the first column */
		int maxLength = names.getFirst().length();
		
		for (String word : map.keySet()) {
			int wordLength = word.length();
			if (wordLength > maxLength) {
				maxLength = wordLength;
			}
		}
		/* Max word length + 1 space */
		++maxLength;
		
		/* For building table */
		StringBuilder builder = new StringBuilder();
		/* Append title */
		builder.append(names.getFirst());
		for (int i = maxLength - names.getFirst().length(); i > 0; --i) {
			builder.append(' ');
		}
		builder.append(names.getSecond());
		builder.append('\n');
		out.print(builder.toString());
		builder.setLength(0);
		
		/* Append data */
		for (Entry<String, T> entry : map.entrySet()) {
			builder.append(entry.getKey());
			for (int i = maxLength - entry.getKey().length(); i > 0; --i) {
				builder.append(' ');
			}
			builder.append(entry.getValue());
			builder.append('\n');
			out.print(builder.toString());
			builder.setLength(0);
		}
	}
	
	/**
	 * Handles the entered data
	 * @param lines Lines to handle
	 * @param map Map to put the handled words in
	 */
	public static void handleProcessData(MyContainer lines,
			HashMap<String, Integer> map) {
		if (lines == null || lines.size() == 0) {
			System.out.println("You have entered nothing.");
			return;
		}
		map.clear();
		
		for (String line : lines) {
			/* Line parser */
			StringTokenizer tokenizer = new StringTokenizer(line);
			
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				/* Increase count */
				map.put(token, map.getOrDefault(token, 0) + 1);
			}
		}
	}
	
	/**
	 * Handles the entered data selectively (If line has more than 10 chars)
	 * @param lines Lines to handle
	 * @param map Map to put the handled words in
	 */
	public static void handleProcessSelectively(MyContainer lines,
			HashMap<String, Integer> map) {
		if (lines == null || lines.size() == 0) {
			System.out.println("You have entered nothing.");
			return;
		}
		map.clear();
		
		for (String line : lines) {
			if (line.length() <= 10) {
				continue;
			}
			
			/* Line parser */
			StringTokenizer tokenizer = new StringTokenizer(line);
			
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				/* Increase count */
				map.put(token, map.getOrDefault(token, 0) + 1);
			}
		}
		if (map.isEmpty()) {
			System.out.println("There were no lines with size > 10.");
		}
	}
}
