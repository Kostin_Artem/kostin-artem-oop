package ua.khpi.oop.kostin06;

/**
 * Helper class with Array functions
 * @author Kostin A.S. CIT-120b
 */
public class ArrayHelper {
	/**
	 * Copies the content of the src array to a new array
	 * @param src Source array
	 * @param newSize New size of the array
	 * @return New array with copies of strings
	 */
	public static String[] copyOf(String[] src, int newSize) {
		if (newSize <= 0) {
			throw new IllegalArgumentException("newSize: " +
                    newSize);
		}
		
		String[] dst = new String[newSize];
		int minLength = src.length < newSize ? src.length : newSize;
		/* System.arraycopy is not in the Arrays helper class =) */
        System.arraycopy(src, 0, dst, 0, minLength);
        return dst;
	}
}
