package ua.khpi.oop.kostin06;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import ua.khpi.oop.kostin03.IoHelper;
import ua.khpi.oop.kostin03.Pair;

/**
 * Main task:
 * - Create dialog menu with next options:
 *  - Input data
 *  - Print the entered data
 *  - Handle data
 *  - Print the result
 *  - Exit etc..
 * - Handle next command line arguments:
 *  - -help - Print a help message
 *  - -debug - The program will print debug messages during processing
 * 
 * @author Kostin A.S. CIT-120b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		/* Console reader */
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(System.in));
		/* Count of each word entry count */
		HashMap<String, Integer> wordsCount = new HashMap<>();
		/* Input lines */
		MyContainer lines = new MyContainer();
		/* Input result (Dialog option number) */
		String readResult = null;
		
		while (true) {
			printMenu();
			
			try {
				readResult = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (readResult == null) {
				System.out.println("The end of stream was encountered.");
				System.out.println("Exiting...");
				return;
			}
			
			switch (readResult) {
			case "1":
				lines.clear();
				if (handleEnter(lines, reader)) {
					System.out.println("The end of stream was encountered.");
					System.out.println("Exiting...");
					return;
				}
				break;
			case "2":
				MyContainer tempLines = handleDeserialize();
				if (tempLines != null) {
					lines = tempLines;
				}
				break;
			case "3":
				handlePrintData(lines);
				break;
			case "4":
				handleSerialize(lines);
				break;
			case "5":
				MyHelper.handleProcessData(lines, wordsCount);
				break;
			case "6":
				MyHelper.handleProcessSelectively(lines, wordsCount);
				break;
			case "7":
				IoHelper.printAsTable(
						new Pair<String, String>("Word name", "Count"),
						wordsCount,
						System.out);
				break;
			case "8":
				handleMagic();
				break;
			case "9":
				System.out.println("Exiting...");
				return;
			default:
				System.out.println("Incorrect argument. Please try again");
				continue;
			}
		}
	}
	
	/**
	 * Prints the dialog menu options
	 */
	private static void printMenu() {
		StringBuilder msg = new StringBuilder();
		msg.append("1) Enter data\n");
		msg.append("2) Deserialize lines from the file \"lines.dat\"\n");
		msg.append("3) Print data\n");
		msg.append("4) Serialize lines to the file \"lines.dat\"\n");
		msg.append("5) Process data\n");
		msg.append("6) Process string if its size > 10\n");
		msg.append("7) Print the result\n");
		msg.append("8) Show me a magic (Woooo) =)\n");
		msg.append("9) Exit\n");
		
		System.out.print(msg.toString());
	}
	
	/**
	 * Handles input
	 * @param lines Container with input lines
	 * @param reader Console reader
	 * @return true if end of file was encountered. false otherwise
	 */
	private static boolean handleEnter(MyContainer lines,
			BufferedReader reader) {
		if (lines == null || reader == null) {
			throw new NullPointerException();
		}
		
		while (true) {
			/* Input line */
			String line = null;
			try {
				line = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line == null) {
				return true;
			} else if (line.isEmpty()) {
				return false;
			}
			lines.add(line);
		}
	}
	
	/**
	 * Prints the input data
	 * @param data Lines to print
	 */
	private static void handlePrintData(MyContainer data) {
		if (data == null || data.size() == 0) {
			System.out.println("You have entered nothing.");
		} else {
			StringBuilder msg = new StringBuilder();
			msg.append("You have entered: \n");
			for (String line : data) {
				msg.append("\"");
				msg.append(line);
				msg.append("\"\n");
			}
			
			System.out.print(msg.toString());
		}
	}
	
	/**
	 * Serializes container into file "lines.dat"
	 * @param lines Input lines
	 */
	private static void handleSerialize(MyContainer lines) {
		if (lines == null) {
			throw new NullPointerException();
		}
		FileOutputStream fos;
		ObjectOutputStream oos;
		/* Output file */
		File file = new File("lines.dat");
		
		if (!file.canWrite() && file.exists()) {
			System.out.println("Can't write to the existing file.");
			return;
		}
		
		try {
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(lines);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Deserializes container from the "lines.dat" file
	 * @return Container with deserialized lines
	 */
	private static MyContainer handleDeserialize() {
		FileInputStream fis;
		ObjectInputStream ois;
		MyContainer list = null;
		/* Input file */
		File file = new File("lines.dat");
		
		if (!file.canRead()) {
			System.out.print("Can't read from the file. ");
			System.out.println("Maybe it not exists.");
			return null;
		}
		
		try {
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);
			list = (MyContainer)ois.readObject();
			ois.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * Demonstrates the use of compare and find functions
	 */
	private static void handleMagic() {
		/* Containers for demonstration */
		MyContainer list1 = new MyContainer();
		MyContainer list2 = new MyContainer();
		StringBuilder msgMagic = new StringBuilder();
		list1.add("Hello");
		list1.add("world!");
		list2.add("Hello");
		list2.add("world!");
		msgMagic.append("The content of the first list:\n");
		msgMagic.append(list1.toString()).append('\n');
		msgMagic.append("The content of the second list:\n");
		msgMagic.append(list2.toString()).append('\n');
		msgMagic.append("Is list1 == list2? ");
		msgMagic.append(list1.equals(list2)).append("!\n\n");
		
		msgMagic.append("Let's add some magic!\n");
		list2.add("Magic!");
		msgMagic.append("The content of the second list:\n");
		msgMagic.append(list2.toString()).append('\n');
		msgMagic.append("Is list1 == list2? ");
		msgMagic.append(list1.equals(list2)).append("!\n\n");
		
		msgMagic.append("Now let's find our world in the second list..\n");
		msgMagic.append("Its index is: ").append(list1.find("world!"));
		msgMagic.append("\nBeautiful! And now let's find the love..\n");
		msgMagic.append("Its index is: ").append(list1.find("love"));
		msgMagic.append("\nLove not found =(\n\n");
		
		msgMagic.append("So close the app and go out the window you ugly!\n");
		msgMagic.append("(Without the negative. Love is always in our hearts)");
		msgMagic.append(" <3\n");
		
		System.out.println(msgMagic.toString());
	}
}
