package ua.khpi.oop.kostin06;

/**
 * Pair of two variables
 * @author Kostin A.S. CIT-120b
 */
public class Pair<T, S> {
	/** First value */
	private T first;
	/** Second value */
	private S second;
	
	/**
	 * Constructor with parameters
	 * @param first First object
	 * @param second Second object
	 */
	public Pair(T first, S second) {
		this.first = first;
		this.second = second;
	}
	
	/**
	 * @return Returns the first object
	 */
	public T getFirst() {
		return first;
	}
	
	/**
	 * @return Returns the second object
	 */
	public S getSecond() {
		return second;
	}
	
	/**
	 * Sets the first object value to value
	 * @param value New value
	 */
	public void setFirst(T value) {
		first = value;
	}
	
	/**
	 * Sets the second object value to value
	 * @param value New value
	 */
	public void setSecond(S value) {
		second = value;
	}
}
