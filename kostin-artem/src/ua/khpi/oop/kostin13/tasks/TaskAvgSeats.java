package ua.khpi.oop.kostin13.tasks;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import ua.khpi.oop.kostin13.Route;

/**
 * Task to find the average seats count
 * @author Kostin A.S. CS-920b
 */
public class TaskAvgSeats implements Callable<Double> {
	/** Routes */
	private ArrayList<Route> routes;
	/** Thread execution timeout */
	private long timeout;
	/** Thread sleep time */
	private long sleepTime;
	
	/**
	 * Constructor with parameters
	 * @param routes Routes
	 * @param timeout Thread execution timeout
	 * @param sleepTime Thread sleep time
	 */
	@SuppressWarnings("unchecked")
	public TaskAvgSeats(ArrayList<Route> routes, long timeout, long sleepTime) {
		if (routes.isEmpty()) {
			throw new IllegalArgumentException("List must not be empty");
		}
		this.routes = (ArrayList<Route>) routes.clone();
		this.timeout = timeout;
		this.sleepTime = sleepTime;
	}
	
	@Override
	public Double call() throws Exception {
		long startTime = System.currentTimeMillis();
		double avg = 0;
		for (Route route : routes) {
			long elapsed = System.currentTimeMillis() - startTime;
			/* Interrupt if timeout reached */
			if (timeout != 0 && elapsed > timeout) {
				throw new InterruptedException("Timeout reached");
			}
			if (sleepTime != 0) {
				Thread.sleep(sleepTime);				
			}
			avg += route.getSeatsCount();
		}
		return avg / routes.size();
	}
}