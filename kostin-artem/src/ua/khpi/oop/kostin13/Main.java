package ua.khpi.oop.kostin13;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.stream.IntStream;

import ua.khpi.oop.kostin13.tasks.TaskMaxSeats;
import ua.khpi.oop.kostin13.tasks.TaskMinSeats;
import ua.khpi.oop.kostin13.tasks.TaskAvgSeats;

/**
 * Main task:
 * - Using the programs of solutions of the previous problems, to demonstrate
 *   the possibility of parallel processing of container elements: to create
 *   at least three additional streams on which to call the appropriate
 *   container processing methods.
 * - Ensure that the user can set the maximum execution time (timeout) at the
 *   end of which processing should stop regardless of whether the final result
 *   is found or not.
 * - For parallel processing, use algorithms that do not change the original
 *   collection.
 * - The number of elements of the container should be large enough, the
 *   complexity of the algorithms for processing the collection should be
 *   comparable, and the execution time is approximately the same, for example:
 *   - search for minimum or maximum;
 *   - calculation of the average value or amount;
 *   - calculation of elements that satisfy a certain condition;
 *   - selection according to the set criteria;
 *   - own version that corresponds to the selected application area
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		// Array to process
		ArrayList<Route> routes = new ArrayList<>();
		fillRouteContainer(routes, 100, 350);
		long timeouts[] = new long[6];
		if (!getTimeoutsFromUser(timeouts)) {
			return;
		}
		// Create tasks (Min/Max/Avg seats count)
		FutureTask<Integer> taskMinSeats = new FutureTask<>(
				new TaskMinSeats(routes, timeouts[0], timeouts[3]));
		FutureTask<Integer> taskMaxSeats = new FutureTask<>(
				new TaskMaxSeats(routes, timeouts[1], timeouts[4]));
		FutureTask<Double> taskAvgSeats = new FutureTask<>(
				new TaskAvgSeats(routes, timeouts[2], timeouts[5]));
		/// Create and run threads
		Thread threads[] = new Thread[3];
		threads[0] = new Thread(taskMinSeats);
		threads[1] = new Thread(taskMaxSeats);
		threads[2] = new Thread(taskAvgSeats);
		System.out.println("[MAIN]: Starting");
		IntStream.range(0, 3).forEach(i -> threads[i].start());
		// Wait for results/interruption
		System.out.println("[MAIN]: Waiting");
		try {
			System.out.printf("[MAIN]: Min seats count: %d\n",
					taskMinSeats.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("[MAIN]: taskMinSeats reached timeout");
		}
		try {
			System.out.printf("[MAIN]: Max seats count: %d\n",
					taskMaxSeats.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("[MAIN]: taskMaxSeats reached timeout");
		}
		try {
			System.out.printf("[MAIN]: Avg seats count: %f\n",
					taskAvgSeats.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("[MAIN]: taskAvgSeats reached timeout");
		}
		System.out.println("[MAIN]: End");
	}
	
	/**
	 * Retrieve timeouts for the tasks from the user
	 * @param timeouts Out parameter
	 * @return Status of the operation. True if it's successful
	 */
	private static boolean getTimeoutsFromUser(long timeouts[]) {
		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in))) {
			/* 3 timeouts and 3 sleep times for this lab */
			for (int i = 0; i < 6; ++i) {
				while (true) {
					System.out.printf("Enter %s for the thread #%d:\n",
							i < 3 ? "timeout" : "sleep time", i);
					String result = in.readLine();
					if (result == null) {
						throw new IOException("Input was interrupted");
					}
					try {
						timeouts[i] = Long.parseLong(result);
						if (timeouts[i] < 0) {
							throw new NumberFormatException(
									"Must be greater than 0");
						}
					} catch (NumberFormatException e) {
						System.out.println(
								"Incorrect number format. Try again");
						continue;
					}
					break;
				}
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * Fill list of routes with random data
	 * @param array Array of routes
	 * @param count Count of routes to add
	 * @param maxSeats Max seats count
	 */
	private static void fillRouteContainer(ArrayList<Route> array, int count,
			int maxSeats) {
		Objects.requireNonNull(array);
		Random rand = new Random();
		EnumSet<DayOfWeek> days = EnumSet.of(DayOfWeek.MONDAY);
		for (int i = 1; i <= count; ++i) {
			array.add(new Route(rand.nextInt(maxSeats), days, i));
		}
	}
}
