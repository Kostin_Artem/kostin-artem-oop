package ua.khpi.oop.kostin13;

import java.util.Objects;

/**
 * Class that represents data of the route node
 * @author Kostin A.S. CS-920b
 */
public final class RouteNode {
	/** Name of the station */
	private String stationName;
	/** Departure time */
	private Time departureTime;
	/** Arrival time */
	private Time arrivalTime;
	/** Free seats count on this node */
	private int freeSeatsCount;
	
	/**
	 * Constructor with parameters
	 * @param name Name of the station
	 * @param dep Departure time
	 * @param arr Arrival time
	 * @param seats Free seats count on the route
	 */
	public RouteNode(String name, Time dep, Time arr, int seats) {
		Objects.requireNonNull(name);
		if (seats < 0 || (dep == null && arr == null)) {
			throw new IllegalArgumentException();
		}
		
		stationName = name;
		departureTime = dep;
		arrivalTime = arr;
		freeSeatsCount = seats;
	}
	
	/**
	 * Copy constructor
	 * @param node RouteNode for copying
	 */
	public RouteNode(RouteNode node) {
		stationName = new String(node.stationName);
		departureTime = new Time(node.departureTime);
		arrivalTime = new Time(node.arrivalTime);
		freeSeatsCount = node.freeSeatsCount;
	}
	
	/**
	 * @return Name of the station
	 */
	public String getStationName() {
		return stationName;
	}
	
	/**
	 * @return Arrival time
	 */
	public Time getArrivalTime() {
		return arrivalTime;
	}
	
	/**
	 * @return Departure time
	 */
	public Time getDepartureTime() {
		return departureTime;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("RouteNode { \"").append(stationName).append("\", ")
		   .append(departureTime).append(", ").append(arrivalTime)
		   .append(", ").append(freeSeatsCount).append(" }");
		return str.toString();
	}
}
