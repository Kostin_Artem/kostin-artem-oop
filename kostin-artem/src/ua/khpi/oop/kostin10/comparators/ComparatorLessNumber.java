package ua.khpi.oop.kostin10.comparators;

import java.util.function.BiPredicate;

import ua.khpi.oop.kostin10.Route;

/**
 * Route comparator (Less number - true)
 * @author Kostin A.S. CS-920b
 */
public final class ComparatorLessNumber implements BiPredicate<Route, Route> {
	@Override
	public boolean test(Route counter, Route border) {
		return counter.getRouteNumber() < border.getRouteNumber();
	}
}
