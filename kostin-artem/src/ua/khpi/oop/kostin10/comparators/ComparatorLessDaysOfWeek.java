package ua.khpi.oop.kostin10.comparators;

import java.util.function.BiPredicate;

import ua.khpi.oop.kostin10.Route;

/**
 * Route comparator (Less days of week count - true)
 * @author Kostin A.S. CS-920b
 */
public class ComparatorLessDaysOfWeek implements BiPredicate<Route, Route> {
	@Override
	public boolean test(Route counter, Route border) {
		return counter.getDaysOfWeek().size() < border.getDaysOfWeek().size();
	}
}
