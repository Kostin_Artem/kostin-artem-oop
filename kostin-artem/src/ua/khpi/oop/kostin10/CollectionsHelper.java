package ua.khpi.oop.kostin10;

import java.util.function.BiPredicate;

/**
 * Algorithms for {@link MyContainer}
 * @author @author Kostin A.S. CS-920b
 */
public final class CollectionsHelper {
	/**
	 * Selection sort for MyContainer
	 * @param <T> Data type
	 * @param cont Container
	 * @param comp Comparator. Returns true if need to swap. First parameter
	 * - counter, second - left border
	 */
	public static <T> void sortSelection(MyContainer<T> cont,
			BiPredicate<T, T> comp) {
		// Array size
		int size = cont.size();
		
		for (int i = 0; i < size; ++i) {
			int target = i;
			
			for (int j = size - 1; j > i; --j) {
				if (comp.test(cont.get(j), cont.get(target))) {
					target = j;
				}
			}
			
			cont.swap(i, target);
		}
	}
}
