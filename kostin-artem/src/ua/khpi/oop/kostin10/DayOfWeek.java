package ua.khpi.oop.kostin10;

import java.io.Serializable;
import java.util.Objects;

/**
 * Enum that represents days of week
 * @author Kostin A.S. CS-920b
 */
public enum DayOfWeek implements Serializable {
	/** Sunday */
	SUNDAY("Воскресенье"),
	/** Monday */
	MONDAY("Понедельник"),
	/** Tuesday */
	TUESTAY("Вторник"),
	/** Wednesday */
	WEDNESDAY("Среда"),
	/** Thursday */
	THURSDAY("Четверг"),
	/** Friday */
	FRIDAY("Пятница"),
	/** Saturday */
	SATURDAY("Суббота");
	
	/** Name of the day of week */
	private final String name;
	
	/**
	 * Constructor with parameters
	 * @param name Name of the day of week
	 */
	DayOfWeek(String name) {
		Objects.requireNonNull(name);
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
