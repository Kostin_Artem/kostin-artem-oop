package ua.khpi.oop.kostin10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.EnumSet;

import ua.khpi.oop.kostin10.comparators.ComparatorLessDaysOfWeek;
import ua.khpi.oop.kostin10.comparators.ComparatorLessNumber;
import ua.khpi.oop.kostin10.comparators.ComparatorLessSeatsCount;

/**
 * Main task:
 * Using the program from laboratory work №9:
 * - Create generic methods for processing collections of objects
 *   according to the task.
 * - Demonstrate the developed functionality (creation, management
 *   and processing of own containers) in dialog and automatic modes.
 * - The automatic execution mode of the program is set by the command line
 *   parameter -auto. For example, "java ClassName -auto".
 * - In the automatic mode the dialogue with the user is absent,
 *   the necessary data are generated, or read from a file.
 * - The use of algorithms from the Java Collections Framework is prohibited.
 * 
 * @author Kostin A.S. CS-920b
 */
public class Main {
	/** Path to the serialization file */
	private static final String S_FILE = "kostin10auto.ser";
	
	/**
	 * Demonstration of the solution
	 * @param args Command line arguments
	 */
	public static void main(String[] args) {
		boolean auto = false;
		if (args.length == 1 && args[0].equals("-auto")) {
			auto = true;
		} else if (args.length > 1){
			System.out.println("Usage: java <program> [-auto]");
			return;
		}
		
		demoSorts();
		System.out.println();
		
		if (auto) {
			demoSolutionAuto();
		} else {
			demoSolution();
		}
	}
	
	/**
	 * Demonstrates solution in a dialog mode
	 */
	private static void demoSolution() {
		System.out.println("Main.demoSolution()");
		MyContainer<Route> cont = new MyContainer<>();
		
		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(System.in))) {
			// Main dialog loop
			loop0:
			while (true) {
				printDlg0Options();
				String line = in.readLine();
				if (line == null) {
					System.out.println("End of stream. Exiting...");
					break;
				}
				
				switch (line) {
				// Print
				case "1": {
					cont.forEach((route) -> {System.out.println(route);});
					break;
				}
				// Add route
				case "2": {
					if (handleAddRoute(cont, in)) {
						break loop0;
					}
					
					break;
				}
				// Sort by number
				case "3": {
					CollectionsHelper.sortSelection(cont,
							new ComparatorLessNumber());
					break;
				}
				// Sort by seats count
				case "4": {
					CollectionsHelper.sortSelection(cont,
							new ComparatorLessSeatsCount());
					break;
				}
				// Sort by days of week count
				case "5": {
					CollectionsHelper.sortSelection(cont,
							new ComparatorLessDaysOfWeek());
					break;
				}
				// Exit
				case "6": {
					System.out.println("Exiting...");
					break loop0;
				}
				default:
					System.out.println("Illegal argument.");
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Handles route creation
	 * @param cont Container to add route into
	 * @param in User input stream
	 * @return true if end of stream encountered
	 * @throws IOException
	 */
	private static boolean handleAddRoute(MyContainer<Route> cont,
			BufferedReader in) throws IOException {
		// Initial values of route (May be edited)
		int number = 0;
		int seats = 0;
		EnumSet<DayOfWeek> days = EnumSet.noneOf(DayOfWeek.class);
		
		// Editing dialog loop
		loop1:
		while (true) {
			printDlg1Options(number, seats, days);
			String line = in.readLine();
			if (line == null) {
				System.out.println("End of stream. Exiting...");
				return true;
			}
			
			switch (line) {
			// Edit number
			case "1": {
				System.out.print("Enter a route number: ");
				// Read a number
				line = in.readLine();
				if (line == null) {
					System.out.println("End of stream. Exiting...");
					return true;
				}
				
				try {
					number = Integer.parseInt(line);
				} catch (NumberFormatException e) {
					System.out.println("Wrong number format.");
					continue; // loop1
				}
				break;
			}
			// Edit seats
			case "2": {
				System.out.print("Enter a seats count: ");
				// Read a number
				line = in.readLine();
				if (line == null) {
					System.out.println("End of stream. Exiting...");
					return true;
				}
				
				try {
					seats = Integer.parseInt(line);
				} catch (NumberFormatException e) {
					System.out.println("Wrong number format.");
					continue; // loop1
				}
				break;
			}
			// Edit days
			case "3": {
				// If value is true - a day will be added
				ArrayList<DayOfWeek> daysToInclude = new ArrayList<>(7);
				
				System.out.print("Enter days indices [0...6] (Sunday = 0): ");
				// Read days indices
				line = in.readLine();
				if (line == null) {
					System.out.println("End of stream. Exiting...");
					return true;
				}
				
				for (int i = 0; i < 7; ++i) {
					if (line.contains(Character.valueOf(
							Character.forDigit(i, 10)).toString())) {
						daysToInclude.add(DayOfWeek.values()[i]);
					}
				}
				
				days = EnumSet.copyOf(daysToInclude);
				break;
			}
			// Add to the container
			case "4": {
				cont.pushBack(new Route(seats, days, number));
				System.out.println("New route added.");
				break loop1;
			}
			default:
				System.out.println("Illegal argument.");
				break;
			}
		}
		
		return false;
	}
	
	/**
	 * Prints options of the main dialog
	 */
	private static void printDlg0Options() {
		StringBuilder msg = new StringBuilder();
		msg.append("1) Print the container\n");
		msg.append("2) Add new route to the container\n");
		msg.append("3) Sort by number\n");
		msg.append("4) Sort by seats count\n");
		msg.append("5) Sort by days of week count\n");
		msg.append("6) Exit\n");
		System.out.print(msg.toString());
	}
	
	/**
	 * Prints options of the route creation dialog
	 * @param number Current route number
	 * @param seats Current route seats count
	 * @param days Current route days
	 */
	private static void printDlg1Options(int number, int seats,
			EnumSet<DayOfWeek> days) {
		StringBuilder msg = new StringBuilder();
		msg.append("1) Edit route number (Current = ")
			.append(number).append(")\n");
		msg.append("2) Edit seats count (Current = ")
		.append(seats).append(")\n");
		msg.append("3) Edit days of week (Current = ")
		.append(days).append(")\n");
		msg.append("4) Apply and add to the container\n");
		System.out.print(msg.toString());
	}
	
	/**
	 * Demonstrates solution in an automatic mode
	 */
	@SuppressWarnings("unchecked")
	private static void demoSolutionAuto() {
		System.out.println("Main.demoSolutionAuto()");
		MyContainer<Route> cont = null;
		
		System.out.println("Deserializing data from file " + S_FILE);
		try (ObjectInputStream stream = new ObjectInputStream(
				new FileInputStream(new File(S_FILE)))) {
			cont = (MyContainer<Route>)stream.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Container data:");
		cont.forEach((route) -> {System.out.println(route);});
		
		System.out.println("\nSort by number:");
		CollectionsHelper.sortSelection(cont, new ComparatorLessNumber());
		cont.forEach((route) -> {System.out.println(route);});
		
		System.out.println("\nSort by seats count:");
		CollectionsHelper.sortSelection(cont, new ComparatorLessSeatsCount());
		cont.forEach((route) -> {System.out.println(route);});
		
		System.out.println("\nSort by days of week count:");
		CollectionsHelper.sortSelection(cont, new ComparatorLessDaysOfWeek());
		cont.forEach((route) -> {System.out.println(route);});
		
		System.out.println("\nExiting...");
	}
	
	/**
	 * Demonstrates sorting algorithm with different comparators
	 */
	private static void demoSorts() {
		System.out.println("Main.demoSorts()");
		MyContainer<Route> cont = new MyContainer<>();
		cont.pushBack(new Route(240, EnumSet.of(DayOfWeek.FRIDAY,
				DayOfWeek.TUESTAY, DayOfWeek.THURSDAY), 5));
		cont.pushBack(new Route(250, EnumSet.of(DayOfWeek.FRIDAY), 4));
		cont.pushBack(new Route(220, EnumSet.of(DayOfWeek.FRIDAY,
				DayOfWeek.TUESTAY), 0));
		cont.pushBack(new Route(60, EnumSet.of(DayOfWeek.FRIDAY,
				DayOfWeek.SATURDAY), 2));
		
		CollectionsHelper.sortSelection(cont, new ComparatorLessSeatsCount());
		System.out.println("- Less seats count:");
		cont.forEach((s) -> {System.out.println(s);});
		
		CollectionsHelper.sortSelection(cont, new ComparatorLessDaysOfWeek());
		System.out.println("- Less days of week:");
		cont.forEach((s) -> {System.out.println(s);});
		
		CollectionsHelper.sortSelection(cont, new ComparatorLessNumber());
		System.out.println("- Less number:");
		cont.forEach((s) -> {System.out.println(s);});
	}
}
