package ua.khpi.oop.kostin12;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Unit tests of the KharkivTransitFilter class
 * @author Kostin A.S. CS-920b
 */
class KharkivTransitFilterTest {
	/**
	 * Tests created filter
	 */
	@Test
	void testFilter() {
		KharkivTransitFilter filter = new KharkivTransitFilter(); 
		Route route = Main.parseRoute("3, 270, Friday, Monday");
		route.addNode(Main.parseRouteNode("Lviv, 18:30:00, 19:00:36, 200"));
		route.addNode(Main.parseRouteNode("Kharkiv, 19:30:44, 21:30:59, 103"));
		route.addNode(Main.parseRouteNode("Boston, 21:56:34, 23:22:02, 28"));
		assertTrue(filter.test(route),
			"Must be true");
		route = Main.parseRoute("3, 270, Friday, Monday");
		route.addNode(Main.parseRouteNode("Lviv, 18:30:00, 19:00:36, 200"));
		route.addNode(Main.parseRouteNode("Boston, 19:30:44, 21:30:59, 103"));
		route.addNode(Main.parseRouteNode("Kharkiv, 21:56:34, 23:22:02, 28"));
		assertFalse(filter.test(route),
		"Must be false. Kharkiv is not transit");
		route = Main.parseRoute("3, 270, Friday");
		route.addNode(Main.parseRouteNode("Lviv, 18:30:00, 19:00:36, 200"));
		route.addNode(Main.parseRouteNode("Kharkiv, 19:30:44, 21:30:59, 103"));
		route.addNode(Main.parseRouteNode("Boston, 21:56:34, 23:22:02, 28"));
		assertFalse(filter.test(route),
		"Must be false. It is only on Friday but not on Monday");
		route = Main.parseRoute("3, 270, Friday, Monday, Saturday, Sunday");
		route.addNode(Main.parseRouteNode("Lviv, 18:30:00, 19:00:36, 200"));
		route.addNode(Main.parseRouteNode("Kharkiv, 19:30:44, 21:30:59, 103"));
		route.addNode(Main.parseRouteNode("Boston, 21:56:34, 23:22:02, 28"));
		assertFalse(filter.test(route),
			"Must be false. Must be only Friday and Monday");
		assertThrows(IllegalArgumentException.class, () -> {
			Route route0 = Main.parseRoute(
				"3, 270, Friday, Monday, Saturday, Sunday");
			route0.addNode(Main.parseRouteNode("Lviv, 07:30:00, 19:00:36, 20"));
		}, "Method must throw exception. Departure time is not evening");
		assertThrows(IllegalArgumentException.class, () -> {
			Route route0 = Main.parseRoute(
				"3, 270, Friday, Monday, Saturday, Sunday");
			route0.addNode(Main.parseRouteNode("Lviv, 23:30:00, 07:00:36, 20"));
		}, "Method must throw exception. Arrival time is not evening");
		
		assertFalse(filter.test(route),
			"Must be false. Must be only Friday and Monday");
	}
}
