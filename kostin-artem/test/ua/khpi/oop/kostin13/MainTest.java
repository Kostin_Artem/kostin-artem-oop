package ua.khpi.oop.kostin13;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.EnumSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ua.khpi.oop.kostin13.tasks.TaskAvgSeats;
import ua.khpi.oop.kostin13.tasks.TaskMaxSeats;
import ua.khpi.oop.kostin13.tasks.TaskMinSeats;

/**
 * Unit tests of the TaskMinSeats, TaskMaxSeats and TaskAvgSeats classes
 * @author Kostin A.S. CS-920b
 */
class MainTest {
	/** Array of routes for testing */
	ArrayList<Route> routes;
	
	/**
	 * Executed before each test
	 */
	@BeforeEach
	void beforeEach() {
		routes = new ArrayList<>();
		routes.add(new Route(4, EnumSet.noneOf(DayOfWeek.class), 0));
		routes.add(new Route(6, EnumSet.noneOf(DayOfWeek.class), 0));
		routes.add(new Route(8, EnumSet.noneOf(DayOfWeek.class), 0));
	}
	
	/**
	 * Tests passing empty lists. Do not repeat at home
	 * @throws Exception
	 */
	@Test
	void testEmpty() throws Exception {
		routes.clear();
		assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			TaskMinSeats task = new TaskMinSeats(routes, 0, 0);
		}, "Method must throw exception. Empty lists are not acceptable");
		assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			TaskMaxSeats task = new TaskMaxSeats(routes, 0, 0);
		}, "Method must throw exception. Empty lists are not acceptable");
		assertThrows(IllegalArgumentException.class, () -> {
			@SuppressWarnings("unused")
			TaskAvgSeats task = new TaskAvgSeats(routes, 0, 0);
		}, "Method must throw exception. Empty lists are not acceptable");
	}
	
	/**
	 * Tests "Find minimum seats count" task
	 * @throws Exception
	 */
	@Test
	void testMin() throws Exception {
		TaskMinSeats task = new TaskMinSeats(routes, 0, 0);
		assertEquals(task.call(), 4,
				"Minimum value is 4");
	}
	
	/**
	 * Tests "Find maximum seats count" task
	 * @throws Exception
	 */
	@Test
	void testMax() throws Exception {
		TaskMaxSeats task = new TaskMaxSeats(routes, 0, 0);
		assertEquals(task.call(), 8,
				"Maximum value is 8");
	}
	
	/**
	 * Tests "Find average seats count" task
	 * @throws Exception
	 */
	@Test
	void testAvg() throws Exception {
		TaskAvgSeats task = new TaskAvgSeats(routes, 0, 0);
		assertEquals(task.call(), 6,
				"Average value is 6");
	}
}
