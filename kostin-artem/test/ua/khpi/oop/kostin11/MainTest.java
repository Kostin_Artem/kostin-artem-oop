package ua.khpi.oop.kostin11;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.EnumSet;

import org.junit.jupiter.api.Test;

/**
 * Unit tests of the parseRoute and parseRouteNode methods
 * @author Kostin A.S. CS-920b
 */
class MainTest {
	/**
	 * Tests parseRoute function
	 */
	@Test
	void testParseRoute() {
		Route route = Main.parseRoute("1, 136, Monday, Wednesday, Friday");
		assertEquals(route.getDaysOfWeek(),
				EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY,
						DayOfWeek.FRIDAY),
				"Days of week must be parsed correctly");
		assertEquals(route.getRouteNumber(), 1,
				"Route number must be parsed correctly");
		assertEquals(route.getSeatsCount(), 136,
				"Seats count must be parsed correctly");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRoute("-1, 136, Monday, Wednesday, Friday");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRoute("1, , Monday, Wednesday, Friday");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRoute("1, 99999999999999999, Monday, Wednesday, Friday");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRoute("1, 136, Wrongday");
		}, "Method must throw exception");
	}
	
	/**
	 * Tests parseRouteNode function
	 */
	@Test
	void testParseRouteNode() {
		RouteNode node = Main.parseRouteNode(
				"Kharkiv, 12:19:30, 17:58:00, 110");
		assertEquals(node.getFreeSeatsCount(), 110,
				"Free seats count must be parsed correctly");
		assertEquals(node.getStationName(), "Kharkiv",
				"Station name must be parsed correctly");
		assertEquals(node.getDepartureTime(), new Time(12, 19, 30),
				"Departure time must be parsed correctly");
		assertEquals(node.getArrivalTime(), new Time(17, 58, 00),
				"Arrival time must be parsed correctly");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRouteNode("Kharkiv, 12:19:30, 17:58:00, 999999999999999");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRouteNode("Kharkiv, 25:00:00, 25:00:00, 0");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRouteNode("Kharkiv, 00:60:00, 00:60:00, 0");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRouteNode("Kharkiv, 00:00:60, 00:00:60, 0");
		}, "Method must throw exception");
		assertThrows(IllegalArgumentException.class, () -> {
			Main.parseRouteNode(", 12:19:30, 17:58:00, 1");
		}, "Method must throw exception");
	}
}
