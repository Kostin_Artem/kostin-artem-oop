package ua.khpi.oop.kostin09;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ua.khpi.oop.kostin09.MyContainer.MyIterator;

/**
 * Unit tests of the MyContainer and MyIterator classes
 * @author Kostin A.S. CS-920b
 */
class MyContainerTest {
	/** Container for tests */
	MyContainer<String> container;
	
	/**
	 * Executed before each test
	 */
	@BeforeEach
	void beforeEach() {
		container = new MyContainer<>();
	}
	
	/**
	 * Tests push, remove, size, empty and iterator's functions
	 */
	@Test
	void testIterator() {
		assertEquals(container.size(), 0,
				"Container must be empty when created");
		for (int i = 1; i <= 4; ++i) {
			container.pushBack(String.valueOf(i));
			assertEquals(container.size(), i,
				"Container size must increase by 1 when pushBack is called");
		}
		container.removeBack();
		assertEquals(container.size(), 3,
				"Container size must be 3 after calling removeBack");
		@SuppressWarnings("rawtypes")
		MyIterator it = container.iterator();
		for (int i = 1; i <= 3; ++i) {
			assertTrue(it.hasNext(), "Iterator must iterate over 3 elements");
			assertEquals(it.next(), String.valueOf(i),
				"Elements must have the same order as it was when all "
				+ "elements were add");
		}
		assertFalse(it.hasNext(),
			"Container must not contain other elements than those that "
			+ "were added");
		assertNull(it.next(), "If there are no more elements then iterator "
			+ "must return null");
		container.removeBack();
		assertEquals(container.size(), 2,
				"Container size must be 2 when the third element was removed");
		container.clear();
		assertEquals(container.size(), 0,
				"Container must be empty after the clear operation");
		container.removeBack();
		assertEquals(container.size(), 0,
				"Container size mustn't go negative if there is no elements");
		container.pushBack("1");
		assertFalse(container.isEmpty(), "Container must not be empty when there"
				+ " are elements in it");
		container.removeBack();
		assertTrue(container.isEmpty(), "Container should be empty when there "
				+ "is no elements in it");
	}
	
	/**
	 * Tests toArray function
	 */
	@Test
	void testToArray() {
		for (int i = 1; i <= 4; ++i) {
			container.pushBack(String.valueOf(i));
			assertEquals(container.size(), i,
				"Container size must increase by 1 when pushBack is called");
		}
		Object[] array = container.toArray();
		for (int i = 1; i <= 4; ++i) {
			assertTrue((String.valueOf(i)).equals(array[i - 1]),
				"Array must have the same elements in the same order");
		}
	}
	
	/**
	 * Tests toString function
	 */
	@Test
	void testToString() {
		for (int i = 1; i <= 4; ++i) {
			container.pushBack(String.valueOf(i));
		}
		assertTrue("MyContainer { 1, 2, 3, 4 }".equals(container.toString()),
			"Сontainer must be stringified correctly");
	}
	
	/**
	 * Tests IO functions using stubs
	 */
	@Test
	void testIO() throws IOException, ClassNotFoundException {
		for (int i = 1; i <= 4; ++i) {
			container.pushBack(String.valueOf(i));
		}
		ArrayList<Object> objects = new ArrayList<>();
		container.writeExternal(new ObjectOutputStub(objects));
		container.clear();
		container.readExternal(new ObjectInputStub(objects));
		@SuppressWarnings("rawtypes")
		MyIterator it = container.iterator();
		for (int i = 1; i <= 4; ++i) {
			assertTrue(it.next().equals(String.valueOf(i)),
				"Сontainer must have the same content after the IO operations");
		}
	}
	
	/**
	 * Stub for the ObjectInput interface
	 * @author Kostin A.S. CS-920b
	 */
	private static class ObjectInputStub implements ObjectInput {
		/** Array for reading objects from it */
		ArrayList<Object> objects;
		
		/**
		 * @param objects Array for reading objects from it
		 */
		public ObjectInputStub(ArrayList<Object> objects) {
			this.objects = objects;
		}
		@Override
		public void readFully(byte[] b) throws IOException {}
		@Override
		public void readFully(byte[] b, int off, int len) throws IOException {}
		@Override
		public int skipBytes(int n) throws IOException {
			return 0;
		}
		@Override
		public boolean readBoolean() throws IOException {
			return false;
		}
		@Override
		public byte readByte() throws IOException {
			return 0;
		}
		@Override
		public int readUnsignedByte() throws IOException {
			return 0;
		}
		@Override
		public short readShort() throws IOException {
			return 0;
		}
		@Override
		public int readUnsignedShort() throws IOException {
			return 0;
		}
		@Override
		public char readChar() throws IOException {
			return 0;
		}
		@Override
		public int readInt() throws IOException {
			return (Integer)objects.remove(0);
		}
		@Override
		public long readLong() throws IOException {
			return 0;
		}
		@Override
		public float readFloat() throws IOException {
			return 0;
		}
		@Override
		public double readDouble() throws IOException {
			return 0;
		}
		@Override
		public String readLine() throws IOException {
			return null;
		}
		@Override
		public String readUTF() throws IOException {
			return null;
		}
		@Override
		public Object readObject() throws ClassNotFoundException, IOException {
			return objects.remove(0);
		}
		@Override
		public int read() throws IOException {
			return 0;
		}
		@Override
		public int read(byte[] b) throws IOException {
			return 0;
		}
		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			return 0;
		}
		@Override
		public long skip(long n) throws IOException {
			return 0;
		}
		@Override
		public int available() throws IOException {
			return 0;
		}
		@Override
		public void close() throws IOException {}
	}
	
	/**
	 * Stub for the ObjectOutput interface
	 * @author Kostin A.S. CS-920b
	 */
	private static class ObjectOutputStub implements ObjectOutput {
		/** Array for writing objects into it */
		ArrayList<Object> objects;
		
		/**
		 * @param objects Array for writing objects
		 */
		public ObjectOutputStub(ArrayList<Object> objects) {
			this.objects = objects;
		}
		@Override
		public void writeUTF(String s) throws IOException {}
		@Override
		public void writeShort(int v) throws IOException {}
		@Override
		public void writeLong(long v) throws IOException {}
		@Override
		public void writeInt(int v) throws IOException {
			objects.add(v);
		}
		@Override
		public void writeFloat(float v) throws IOException {}
		@Override
		public void writeDouble(double v) throws IOException {}
		@Override
		public void writeChars(String s) throws IOException {}
		@Override
		public void writeChar(int v) throws IOException {}
		@Override
		public void writeBytes(String s) throws IOException {}
		@Override
		public void writeByte(int v) throws IOException {}
		@Override
		public void writeBoolean(boolean v) throws IOException {}
		@Override
		public void writeObject(Object obj) throws IOException {
			objects.add(obj);
		}
		@Override
		public void write(byte[] b, int off, int len) throws IOException {}
		@Override
		public void write(byte[] b) throws IOException {}
		@Override
		public void write(int b) throws IOException {}
		@Override
		public void flush() throws IOException {}
		@Override
		public void close() throws IOException {}
	}
}
