# Лабораторна робота №11. Регулярні вирази. Перевірка даних
Мета: Ознайомлення з принципами використання регулярних виразів для перевірки рядка на відповідність шаблону.

## 1 Вимоги
### 1.1	Розробник
- Костін Артем Сергійович
- Студент групи КН-920б
- Дата розробки: 18 лютого 2022

### 1.2 Загальне завдання
- Продемонструвати ефективне (оптимальне) використання регулярних виразів для перевірки коректності (валідації) даних, що вводяться, перед записом в domain-об'єкти відповідно до призначення кожного поля для заповнення розробленого контейнера:
  - При зчитуванні даних з текстового файла в автоматичному режимі;
  - При введенні даних користувачем в діалоговому режимі.

### 1.3 Задача
- Квиткова каса:
    - Дані про маршрут:
        - Маршрут - необмежений набір значень у вигляді:
            - Назва станції
            - Час прибуття (Для проміжних і кінцевої)
            - Час відправлення (Для початкової та проміжних)
            - Кількість вільних місць
        - Загальна кількість місць
        - Дні тижня
        - Номер рейсу.

---

## 2 Опис програми

### 2.1 Засоби ООП
- Декомпозиція - метод, що дозволяє замінити вирішення одного великого завдання рішенням серії менших завдань
- Доменні об'єкти - це об'єкти в об'єктно-орієнтованих комп'ютерних програмах, що виражають сутності з моделі предметної області, що відноситься до програми, та реалізують бізнес-логіку програми
- Generic methods / classes Java дозволяють програмістам вказати, за допомогою одного оголошення методу, набір пов’язаних методів або за допомогою одного оголошення класу набір пов’язаних типів відповідно.

### 2.2 Ієрархія та структура класів
**Структура проекту**

```
kostin11
├── DayOfWeek.java
├── Main.java
├── MyContainer.java
├── MyNode.java
├── Route.java
├── RouteNode.java
├── Time.java
└── package-info.java
```

### 2.3 Важливі фрагменти програми
**Призначення та структура розроблених методів**

```java
public static void main(String[] args)
```

Призначення: головна функція

Опис роботи:
- Вирішує головне та індивідуальне завдання. Детальніше про алгоритм розв’язання кожного завдання можна дізнатись із розділу 2.3 Важливі елементи програми

Аргументи:
- args - Параметри командного рядка

---

```java
private static void demoSolution()
```

Призначення: Демонструє роботу в діалоговому режимі

Опис роботи:
- Створює буфер вводу
- Виводить меню
- Запитує наступний крок у користувача
- Оброблює наступний крок

---

```java
private static void demoSolutionAuto()
```

Призначення: Демонструє роботу в автоматичному режимі

Опис роботи:
- Десеріалізує контейнер
- Демонструє алгоритми сортування та виводить зміст контейнера

---

```java
private static boolean handleAddRoute(MyContainer<Route> cont,
        BufferedReader in) throws IOException
```

Призначення: Керує діалогом додавання нового маршруту до контейнера

Опис роботи:
- Виводить меню
- Запитує наступний крок у користувача
- Оброблює наступний крок

Аргументи:
- cont - Контейнер, в який додається маршрут
- in - Буфер вводу для спілкування з користувачем

---

```java
private static Route parseRoute(String line)
        throws IllegalArgumentException
```

Призначення: Зчитує маршрут з рядка за допомогою RegEx

Опис роботи:
- Компілює регулярний вираз
- Отримує дані з рядка
- Повертає зчитаний маршрут

Аргументи:
- line - Рядок з даними маршрута

---

```java
private static RouteNode parseRouteNode(String line)
        throws IllegalArgumentException
```

Призначення: Зчитує станцію з рядка за допомогою RegEx

Опис роботи:
- Компілює регулярний вираз
- Отримує дані з рядка
- Повертає зчитаний маршрут

Аргументи:
- line - Рядок з даними станції

---

**Розроблені класи**

Фрагменти коду функції demoSolution подано на рисунку 1

![](assets/demoSolution.png)

Рисунок 1 - Фрагмент коду функції demoSolution

---

Фрагменти коду функції demoSolutionAuto подано на рисунку 2

![](assets/demoAuto.png)

Рисунок 2 - Фрагмент коду функції demoSolutionAuto

---

Фрагменти коду функції addRoute подано на рисунках 3-4

![](assets/addroute0.png)

Рисунок 3 - Фрагмент коду функції addRoute

---

![](assets/addroute1.png)

Рисунок 4 - Фрагмент коду функції addRoute

---

Фрагменти коду функції parseRoute подано на рисунку 5

![](assets/parseroute.png)

Рисунок 5 - Фрагмент коду функції parseRoute

---

Фрагменти коду функції parseNode подано на рисунку 6

![](assets/parsenode.png)

Рисунок 6 - Фрагмент коду функції parseNode

---

**UML-діаграма**

Діаграму розроблених класів подано на рисунку 7

![#TODO Diagram](assets/uml11.png)

Рисунок 7 - Діаграма розроблених класів

---

## 3 Варіанти використання
Для демонстрації результатів кожної задачі використовується виконання програми у консолі. На рисунку 8 зображені результати роботи програми в автоматичному режимі

![](assets/result.png)

Рисунок 8 - Результати роботи програми в автоматичному режимі

---

На рисунку 9 зображені вхідні дані для програми в автоматичному режимі

![](assets/data.png)

Рисунок 9 - Вхідні дані для програми в автоматичному режимі

---

## Висновки
Ознайомився з принципами використання регулярних виразів для перевірки рядка на відповідність шаблону.
